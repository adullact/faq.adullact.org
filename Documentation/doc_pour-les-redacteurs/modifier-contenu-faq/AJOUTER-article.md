# Ajouter un article à la FAQ

## Accéder aux articles sur GitLab

L'intégralité du contenu du site de la FAQ est disposé sur le [dépôt FAQ.adullact.org](https://gitlab.adullact.net/adullact/faq.adullact.org) :

Les articles sont contenus dans le dossier **FAQ_articles**.

![Arborescence - Racine](../images/gitlab/arborescence-racine.png)

Une fois dans ce dossier, il suffit de se rendre là où l'article à ajouter doit être placé.

Pour retourner en arrière dans l'arborescence, il suffit de se référer au **fil d'Ariane** situé au-dessus des fichiers et de cliquer sur le dossier où l'on souhaite retourner :

![Fil d'Ariane](../images/gitlab/fil-ariane.png)

## Ajouter l'article

Une fois placé dans le dossier où l'on souhaite ajouter l'article, il suffit de cliquer sur le bouton `+` situé au-dessus de la liste des fichiers
contenus dans le dossier où l'on est placé. Plusieurs propositions s'affichent, pour ajouter un nouveau fichier, il suffit alors de cliquer
sur `New file` :

![New file](../images/gitlab/ajouter-article.png)

Une **fenêtre d'édition** va alors s'ouvrir, permettant de rédiger l'article, ou d'y ajouter un contenu (copier-coller) :

![Fenêtre de rédaction](../images/gitlab/new-file.png)

### Bien nommer un fichier

Au moment de la création de l'article, il faut nommer le fichier correspondant. L'encadré se trouve **en haut à gauche** de la fenêtre de rédaction :

![Nommer le fichier](../images/gitlab/nommer-un-article.png)

Le nom du fichier va se retrouver **dans l'URL** correspondant sur le site :

![Nommer le fichier](../images/gitlab/url-article.png)

### Rédiger l'article

L'article est divisé en deux parties. En haut se trouve l'en-tête, qui permet de récupérer les informations essentielles de l'article (titre, description, tags, date, ...). Cet en-tête doit être ajouté à chaque nouvel article :

```TOML
+++
title = "Titre"
description = "Description"
tags = [ "Tag1", "Tag2", "Tag3" ]
date = "AAAA-MM-JJ"
draft = true
+++
{{< toc >}}
```

La ligne `draft = true` permet de garder l'article en mode brouillon pour qu'il ne soit pas publié sur le site.

Pour **publier** l'article une fois celui-ci terminé, il suffit de supprimer la ligne `draft =  true` ou de remplacer `true` par `false`.

Pour en savoir plus sur les **règles syntaxiques** propres au MarkDown, une documentation est disponible en cliquant sur ce [lien](./syntaxe-redaction-articles/README.md).

## Valider les modifications

Une fois la modification effectuée, il faut l'enregistrer dans le projet par un **commit** pour conserver l'historique des modifications.

Le commit se fait sur la page d'édition, dans l'encadré situé en-dessous de l'article :

![Commit](../images/gitlab/commit-message.png)

Ce message doit contenir une brève explication de ce qui a été modifié, et doit être fait sur la branche **main**.

Une fois les modifications faites, il suffit de cliquer sur le bouton **Commit changes** pour que les modifications soient prises en compte et ajoutées sur le site de la FAQ :

![Valider les modification](../images/gitlab/valider-commit.png)

## Processus de publication

Une fois les modifications validées et le commit confirmé, les modifications vont se déployer automatiquement sur le [site de la FAQ](https://faq.adullact.org/).

Pour suivre l'évolution du processus : <https://gitlab.adullact.net/adullact/faq.adullact.org/-/pipelines>

Il suffit de **cliquer** sur la dernière _pipeline_ (instructions qui se déroulent en plusieurs étapes) en cours :

![Pipeline en cours](../images/gitlab/suivre-processus-ci-cd.png)

### Les voyants sont verts

Si tous les voyants de droite sont en vert, la modification est publiée, il n'y a rien de plus à faire :

![CI/CD verte](../images/gitlab/ci-cd-verte.png)

### Tous les voyants ne sont pas verts

Si les étapes ne sont pas toutes vertes, plusieurs possibilités :

#### L'erreur est le markdown linter

S'il s'agit du Markdown linter, le problème vient de l'article modifié qui ne respecte pas les règles de mise en forme et/ou syntaxiques du markdown (cf. [documentation pour la rédaction](../syntaxe-redaction-articles/regles-redaction-markdown.md)) :

![CI/CD MD rouge](../images/gitlab/ci-cd-lint-md-rouge.png)

#### L'erreur n'est pas le markdown linter

Dans ce cas, il faut **contacter les développeurs** pour qu'ils puissent regarder d'où vient l'erreur.
