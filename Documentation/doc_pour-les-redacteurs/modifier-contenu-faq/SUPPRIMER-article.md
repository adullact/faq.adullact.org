# Supprimer un article de la FAQ

Pour supprimer un article, plutôt que d'effacer le fichier correspondant, il faut le dépublier.

En faisant cela, les fichiers sont conservés, mais n'apparaissent plus dans la FAQ.

## Accéder aux articles sur GitLab

L'intégralité du contenu du site de la FAQ est disposé sur le [dépôt FAQ.adullact.org](https://gitlab.adullact.net/adullact/faq.adullact.org) :

Les articles sont contenus dans le dossier **FAQ_articles**.

![Arborescence - Racine](../images/gitlab/arborescence-racine.png)

Une fois dans ce dossier, il suffit de se rendre là où est l'article que l'on souhaite dépublier, et de le sélectionner pour l'afficher.

Pour retourner en arrière dans l'arborescence, il suffit de se référer au fil d'Ariane situé au-dessus des fichiers et de cliquer sur le dossier où l'on souhaite retourner :

![Fil d'Ariane](../images/gitlab/fil-ariane.png)

## Dépublier l'article

### Ouvrir l'article en mode édition

Une fois affiché, l'article doit être modifié pour pouvoir être dépublié. Pour le modifier, il faut cliquer sur le bouton **Edit** en haut à droite de celui-ci :

![Editer un article](../images/gitlab/editer-un-article.png)

## Ajouter la date d'expiration

Pour dépublier un article sur la FAQ, il faut ajouter une date d'expiration de celui-ci. À partir de la date donnée, l'article ne sera plus visible sur la FAQ.

Pour cela, il faut ajouter la fonction `expirydate` dans l'en-tête de l'article correspodant :

| Avant | Après |
|:-----:|:-----:|
| ![En-tête d'un article - Hugo](../images/gitlab/en-tete-article.png) | ![En-tête d'un article - Hugo](../images/gitlab/apres-modification-en-tete.png) |

Pour être prise en compte, cette fonction doit avoir une date, sous format AAAA-MM-JJ :

```TOML
expirydate = "2021-07-31"
```

## Valider les modifications

Une fois la modification effectuée, il faut l'enregistrer dans le projet par un **commit** pour conserver l'historique des modifications.

Le commit se fait sur la page d'édition, dans l'encadré situé en-dessous de l'article :

![Commit](../images/gitlab/commit-message.png)

Ce message doit contenir une brève explication de ce qui a été modifié, et doit être fait sur la branche **main**.

Une fois les modifications faites, il suffit de cliquer sur le bouton **Commit changes** pour que les modifications soient prises en compte et ajoutées sur le site de la FAQ :

![Valider les modification](../images/gitlab/valider-commit.png)

## Processus de publication

Une fois les modifications validées et le commit confirmé, les modifications vont se déployer automatiquement sur le [site de la FAQ](https://faq.adullact.org/).

Pour suivre l'évolution du processus : <https://gitlab.adullact.net/adullact/faq.adullact.org/-/pipelines>

Il suffit de **cliquer** sur la dernière _pipeline_ (instructions qui se déroulent en plusieurs étapes) en cours :

![Pipeline en cours](../images/gitlab/suivre-processus-ci-cd.png)

### Les voyants sont verts

Si tous les voyants de droite sont en vert, la modification est publiée, il n'y a rien de plus à faire :

![CI/CD verte](../images/gitlab/ci-cd-verte.png)

### Tous les voyants ne sont pas verts

Si les étapes ne sont pas toutes vertes, plusieurs possibilités :

#### L'erreur est le markdown linter

S'il s'agit du Markdown linter, le problème vient de l'article modifié qui ne respecte pas les règles de mise en forme et/ou syntaxiques du markdown (cf. [documentation pour la rédaction](../syntaxe-redaction-articles/regles-redaction-markdown.md)) :

![CI/CD MD rouge](../images/gitlab/ci-cd-lint-md-rouge.png)

#### L'erreur n'est pas le markdown linter

Dans ce cas, il faut **contacter les développeurs** pour qu'ils puissent regarder d'où vient l'erreur.
