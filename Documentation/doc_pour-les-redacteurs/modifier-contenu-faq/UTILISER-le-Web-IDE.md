# Utiliser le Web IDE de GitLab

## Qu'est-ce qu'un IDE ?

Il s'agit d'un **environnement de développement intégré**, permettant dans notre cas de modifier **plusieurs fichiers du site en même temps**.

## Dans quel cas utiliser l'IDE ?

Si un rédacteur souhaite modifier plusieurs articles au même moment, plutôt que de les modifier un par un et de valider chaque modification
par un **commit**, le Web IDE va permettre de modifier **plusieurs fichiers** avant d'envoyer les modifications sur le site.

## Ouvrir le Web IDE

Sur la **page principale** du [dépôt de la FAQ](https://gitlab.adullact.net/adullact/faq.adullact.org) :

![Ouvrir le Web IDE](../images/gitlab/ouvrir-web-ide.png)

La **fenêtre du Web IDE s'ouvre**, et pour trouver les articles, il suffit d'aller dans `FAQ_articles/` :

![Web IDE](../images/gitlab/web-ide.png)

Ce qui aura pour effet de **dérouler** l'arborescence du dossier :

![Web IDE - FAQ_Articles](../images/gitlab/web-ide-faq-articles.png)

Il suffit ensuite de **sélectionner** un article pour l'afficher et le modifier.

## Déplacer un article

Il est directement possible de **déplacer** un article via l'IDE direcement. Pour cela, il faut cliquer sur les **trois points situés à droite** du dossier ou de l'article à déplacer, puis sur `Rename/move` :

![Déplacer un article](../images/gitlab/move-article-ide.png)

Une fenêtre contenant le **chemin actuel** du dossier ou de l'article apparaît, et pour le déplacer, il suffit de **modifier le dossier** dans lequel il se trouve, pour l'amener vers le nouveau dossier (existant ou non), et de valider :

![Modifier le chemin](../images/gitlab/chemin-move-article.png)

## Valider les modifications

Une fois tous les articles modifiés, il faut valider ces modifications dans un **commit** via le bouton situé sous l'arborescence du projet :

![Valider les modifications](../images/gitlab/valider-modif-web-ide.png)

La page montrant les **fichiers modifiés** et les **modifications faites** s'affiche. Après avoir vérifié que les modifications sont correctes, il faut les valider :

![Commit](../images/gitlab/modif-commit-web-ide.png)

Le message de commit peut être **modifié**, mais il doit dans tous les cas contenir l'information de ce qui a été modifié :

![Commit](../images/gitlab/modif-commit-valid-web-ide.png)

Pour assurer une **dernière vérification avant publication**, laissez les cases `Create a new branch` et `Start a new merge request` sélectionnées.

Cela permettra d'ouvrir une **requête de fusion**, qui pourra être validée par un relecteur, et sera ensuite **fusionnée** dans la branche principale et **publiée** sur le site de la FAQ.

Une fois que tout est bon, on **valide** en cliquant sur le bouton `Commit` :

![Commit](../images/gitlab/modif-valid-web-ide.png)

## Créer une Merge Request

### Qu'est-ce qu'une Merge Request

Une Merge Request (ou _demande de fusion_), et un principe propre à GitLab permettant de **travailler sur une copie du projet**, sans toucher à son contenu directement.

Lorsqu'une modification est validée par un commit, **ce commit va créer une branche**, qui est une copie du contenu de la branche principale (dans notre cas `dev`) avec les modifications en plus.

Pour pouvoir **rappatrier** les modifications de la branche créée vers la branche principale, il faut alors faire une Merge Request.

### Création de la Merge Request

Une nouvelle page va alors s'ouvrir, pour **créer une Merge Request**. Il s'agit d'une requête permettant de **vérifier les modifications** avant de les publier directement sur le site :

![Nouvelle MR](../images/gitlab/mr-web-ide.png)

Sur cette page, il est possible d'**ajouter une description, ainsi qu'un titre** pour que la personne qui s'occupera de la **relecture** sache de quoi il s'agit.

Une fois que tout est bon, il suffit de valider la création de la merge request :

![Valider la création de la MR](../images/gitlab/valider-create-mr.png)

## Valider la Merge Request

Une fois la merge request créée, celle-ci peut être **validée par un tierce personne**. Une fois celle-ci **approuvée**, si la _pipeline_ est verte, il suffit de cliquer sur `Merge`.

![Vérifier la MR](../images/gitlab/verifier-mr.png)

Les modifications sont désormais ajoutées dans la branche principale, et le déploiement sur **[FAQ.adullact.org](https://faq.adullact.org/)** est en cours (celui-ci prend environ deux minutes).

### Faire un Rebase

Si plusieurs personnes travaillent en même temps sur le dépôt, il est possible que la Merge Request sur laquelle le rédacteur travaille nécessite un Rebase.

À la place de `Merge` apparaît alors `Rebase` :

![Rebase](../images/gitlab/rebase.png)

Une MR nécessite un rebase lorsque la branche sur laquelle le rédacteur a travaillé n'est plus à jour par rapport à la branche principale.

Une fois le rebase fait, le bouton `Merge` réapparaît et le rédacteur peut fusionner son travail dans la branche principale.
