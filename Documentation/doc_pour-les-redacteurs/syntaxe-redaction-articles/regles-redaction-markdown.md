# Règles de syntaxe pour rédiger un article en _Markdown_

## Exemple - Sous-Titre

```MD
## Exemple - Sous-Titre
```

### Exemple - Sous-sous-Titre

```MD
### Exemple - Sous-sous-Titre
```

Et prima post Osdroenam quam, ut dictum est, ab hac descriptione discrevimus, Commagena, nunc Euphratensis, clementer adsurgit, Hierapoli, vetere Nino et Samosata civitatibus amplis inlustris.

Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis sudoribus induratae.

## Exemple - Gras et italique

**Et prima post Osdroenam quam, ut dictum est, ab hac descriptione discrevimus, Commagena, nunc Euphratensis, clementer adsurgit, Hierapoli, vetere Nino et Samosata civitatibus amplis inlustris.**

```MD
**Et prima post Osdroenam quam, ut dictum est, ab hac descriptione discrevimus, Commagena, nunc Euphratensis, clementer adsurgit, Hierapoli, vetere Nino et Samosata civitatibus amplis inlustris.**
```

_Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis sudoribus induratae._

```MD
_Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis sudoribus induratae._
```

## Exemple - Liste à puces

Pour créer une liste numérotée, il vous suffira
de commencer la ligne par un tiret. Pour créer plusieurs niveaux, il faut ajouter deux espaces avant le tiret :

- Liste 1
  - Liste 1.1
    - Liste 1.1.1
- Liste 2
- Liste 3

```MD
- Liste 1
  - Liste 1.1
    - Liste 1.1.1
- Liste 2
- Liste 3
```

## Exemple - Liste numérotée

Pour créer une liste numérotée, il vous suffira
de commencer la ligne par un chiffre suivi d’un point.

1. Liste 1
2. Liste 2
3. Liste 3

```MD
1. Liste 1
2. Liste 2
3. Liste 3
```

## Exemple - Citation

> Les citations se font avec le signe > \
> et s'affichent sous forme de texte en retrait

```MD
> Les citations se font avec le signe >
> et s'affichent sous forme de texte en retrait
```

## Exemple - Liens externes

[ADULLACT](https://adullact.org/ "Ceci est un lien vers le site de l'ADULLACT")

```MD
[ADULLACT](https://adullact.org/ "Ceci est un lien vers le site de l'ADULLACT")
```

[Plus d'exemples.](./liens.md)

## Exemple - Tableau simple

|cellule 1|cellule 2|
|---------|---------|
|    A    |    B    |
|    C    |    D    |

```MD
|cellule 1|cellule 2|
|---------|---------|
|    A    |    B    |
|    C    |    D    |
```

## Exemple - Tableau avec alignement du texte

| Aligné à gauche  | Centré | Aligné à droite |
|:-----------------|:------:|----------------:|
| Gauche | Centré | Droite |
| Gauche | Centré | Droite |
| Gauche | Centré | Droite |

```MD
| Aligné à gauche  | Centré | Aligné à droite |
|:-----------------|:------:|----------------:|
| Gauche | Centré | Droite |
| Gauche | Centré | Droite |
| Gauche | Centré | Droite |
```

## Autres règles

- Il faut ajouter une **ligne vide** à la fin de la page.
- Une ligne ne doit pas dépasser les **300 caractères**.
- Un titre ne contient **pas de ponctuation** en fin de ligne
- Il ne faut **pas d'espace** en fin de ligne
