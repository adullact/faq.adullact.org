# Ajouter une image

## Où placer les images

Pour ajouter une image sur un article, il faut d'abord l'ajouter en local dans le projet.

Les images sont placées dans le dossier `FAQ_ressources/images/` :

![FAQ_ressources](../images/gitlab/dossier-ressources-images.png) ![images](../images/gitlab/dossier-images.png)

Les images sont rangées en fonctions du sujet concerné.

## Ajouter une image visible

![Exemple](../images/gitlab/exemple-image.png)

```MD
![Exemple](../images/gitlab/exemple-image.png)
```

## Ajouter une image en lien cliquable

[Texte alternatif](../images/gitlab/exemple-image.png)

```MD
[Texte alternatif](../images/gitlab/exemple-image.png)
```
