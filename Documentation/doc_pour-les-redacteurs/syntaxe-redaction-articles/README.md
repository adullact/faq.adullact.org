# Syntaxe pour rédiger un article (_Markdown_)

- Utiliser les listes à puces/numéros
- Utiliser la hiérarchie des titres
- Mettre en gras/italique
- [Ajouter des **images**](images.md)
- [Ajouter des **liens**](liens.md)
