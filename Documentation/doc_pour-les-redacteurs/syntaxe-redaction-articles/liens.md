# Ajouter un lien dans un article


## Liens externes (URLs)

```markdown
[Lien externe vers un autre site web](https://adullact.org)
```

## Liens internes (site web FAQ)



### Liens vers un tag de la FAQ

Par exemple, pour ajouter un lien vers le tag [licences libres](https://faq.adullact.org/tags/licence-libre/) :

```markdown
[licences libres](/tags/licence-libre/)
```

### Liens vers un article de la FAQ

Pour le site web FAQ, un article = dossier.

```markdown
FAQ_articles/  <--- dossier racine des articles de la FAQ
├── autre-dossier-A/
│   ├── sous-dossier-A1/
│   └── sous-dossier-A2/
└── dossier-parent/
    ├── dossier-voisin/
    └── dossier-actuel/
        ├── sous-dossier/
        └── _index.md  ----> fichier que vous êtes en train d'éditer
```

```markdown
[Liens vers un sous-dossier](sous-dossier/)
[Liens vers le dossier parent](../)
[Liens vers un vers un dossier voisin](../dossier-voisin/)
[Liens vers un vers un dossier "autre-dossier-A"](../../autre-dossier-A/)
[Liens vers un vers un dossier "sous-dossier-A2"](../../autre-dossier-A/sous-dossier-A2/)
```

Il faut placer entre les `()` le chemin relatif allant vers le repertoire de l'article à relier.

## Exemple

Ceci est un passage d'un article contenant plusieurs liens internes.
Le chemin de cet article est `FAQ_articles/juridique/choisir-licence/compatibilite-entre-licences/_index.md`

```markdown
+++
title = "Mécanisme de la compatibilité entre licences"
description = "Le mécanisme de la compatibilité entre licences"
tags = [ "Juridique", "Licence libre" ]
date = "2014-04-07"
+++
{{< toc >}}

...

[GPLv3](../presentation-licences/gpl-v3/)
```

L'article où le lien mène est situé dans un **dossier différent**. Pour pouvoir reculer d'un cran dans l'arborescence,
il faut donc placer `../` en début de chemin.

On peut le placer **autant de fois que nécessaire** pour remonter d'autant de niveau que nécessaire :

Exemple, pour remonter de 2 niveaux : `../../`
