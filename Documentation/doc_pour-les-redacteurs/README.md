# Documentation pour les _Rédacteurs_

- [Modifier le contenu du site web](./modifier-contenu-faq/README.md)
  - [**Modifier** un article](./modifier-contenu-faq/MODIFIER-article.md)
  - [**Ajouter** un article](./modifier-contenu-faq/AJOUTER-article.md)
  - [**Supprimer** un article](./modifier-contenu-faq/SUPPRIMER-article.md)
  - [Utiliser le **Web IDE**](./modifier-contenu-faq/UTILISER-le-Web-IDE.md)
- [Syntaxe pour rédiger les articles](./syntaxe-redaction-articles/README.md)  (_Markdown_)
  - [Syntaxe générale](./syntaxe-redaction-articles/regles-redaction-markdown.md)
  - [Ajouter des **images**](./syntaxe-redaction-articles/images.md)
  - [Ajouter des **liens**](./syntaxe-redaction-articles/liens.md)
