# Prerequisites

- Git
- [Hugo](https://gohugo.io/getting-started/installing/)
- prerequisites for [`adullact/theme-hugo_faq.adullact.org`](https://gitlab.adullact.net/adullact/theme-hugo_faq.adullact.org)
  GIT submodule
  - [npm](https://docs.npmjs.com/)  (Node Package Manager)
  - [npx](https://www.npmjs.com/package/npx)  (Node Package eXecute) available in **npm** starting version 5.2
  - bzip2

## Install or update HUGO on Ubuntu (18.04 or 20.04)

We recommend using same version of HUGO as  version used by Gitlab CI.
See: [`.gitlab/ci/job.build.gitlab-ci.yml`](https://gitlab.adullact.net/adullact/faq.adullact.org/-/blob/main/.gitlab/ci/job.build.gitlab-ci.yml)

```bash
# Install or update HUGO
HUGO_VERSION: "0.86.1"
HUGO_URL: "https://github.com/gohugoio/hugo/releases/download"
wget "${HUGO_URL}/v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.deb"
sudo dpkg -i hugo_extended_${HUGO_VERSION}_Linux-64bit.deb
```

## Install prerequisites for `adullact/theme-hugo_faq.adullact.org` on Ubuntu (18.04 or 20.04)

```bash
# Install NPM, NPX and bzip2
sudo apt-get update
sudo apt-get -y --no-install-recommends  bzip2  npm
```

For developers with multiple projects using NodeJS and NPM,
we strongly recommend using a Node version manager like [nvm](https://github.com/nvm-sh/nvm)
(Node Version Manager) to install Node.js and npm.
