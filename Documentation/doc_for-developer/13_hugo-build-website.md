# Build website with **HUGO**

You should have already done these steps:

- [Prerequisites](10_prerequisites.md)
- [Clone repository and load GIT submodule](11_git-clone_with-submodule.md)
- [Install theme NPM packages and compile assets with GULP](12_hugo-theme_build-assets.md)

```bash
cd faq.adullact.org/  # your project directory
cd .gitlab/website_hugo/

# Build website and provide a local webserver
hugo server
     # By default, Web Server is available at http://localhost:1313/

# Build website (include expired content)
hugo server --buildExpired

# Build website (include draft content)
hugo server --buildDrafts
```

see: [Hugo server documentation](https://gohugo.io/commands/hugo_server/)
