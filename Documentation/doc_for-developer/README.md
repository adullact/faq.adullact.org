# Documentation for _Developper_ and _Administrator_

## How to build website on developer computer

- [Prerequisites](10_prerequisites.md)
- [Clone repository and load GIT submodule](11_git-clone_with-submodule.md)
- [Install theme NPM packages and compile assets with GULP](12_hugo-theme_build-assets.md)
- [Build website with **HUGO**](13_hugo-build-website.md)

## How to update HUGO theme

- **@@@TODO** "update HUGO theme"
- [To update Git submodule of theme on main repository](21_hugo-theme_update-git-submodule.md)

## QA tools

- [QA - Open-source softwares](QA_tools/README.md)
- [QA - Oline tools for **DEV** website](QA_tools/QA_online-tools_for-DEV-website.md)
- [QA - Oline tools for **PROD** website](QA_tools/QA_online-tools_for-PROD-website.md)

## Technical organization of this project **@@@TODO**

- Directories organization
- CI organization
- ...

## How to upload Docker image to Gitlab Registry

**Docker** images of prerequisites usign by **Gitlab CI**:

- [**Hugo** build with **hugo-geekdoc** theme](../../.gitlab/ci/docker/build_hugo_prerequisites/)
- [**deploy** website with **rsync**](../../.gitlab/ci/docker/deploy_rsync/)
