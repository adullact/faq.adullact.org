<?php

$cspAlgo = 'sha256';
$matomoUrl = 'https://statistiques.adullact.org/';
$matomoWebsiteId = 14; // DEV website
$matomoWebsiteId = 7;  // PROD website
$matomoJs = "
var _paq = window._paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
function embedTrackingCode(){
    var u='${matomoUrl}';
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '${matomoWebsiteId}']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
}
if (window.addEventListener){ window.addEventListener('load', embedTrackingCode, false); }
else if (window.attachEvent){ window.attachEvent('onload', embedTrackingCode);           }
else                        { embedTrackingCode();                                       }
";


////////////////////////////////////////////////
$outputMatomoJs = trim("$matomoJs");
$outputMatomoJsMinified = trim("$matomoJs");
$outputMatomoJsMinified =  preg_replace('/\r\n/', '\n', $outputMatomoJsMinified);
$outputMatomoJsMinified = str_replace("   ",  " ","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("   ",  " ","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("  ",  " ","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("  ",  " ","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("  ",  " ","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("\n", "","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("\t", "","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("\r", "","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("if (", "if(","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("{ ", "{","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace("; ", ";","$outputMatomoJsMinified");
$outputMatomoJsMinified = str_replace(", ", ",","$outputMatomoJsMinified");
$outputMatomoJsMinified = trim("$outputMatomoJsMinified");

$hashOfMatomoJs = base64_encode(hash("$cspAlgo", "\n$outputMatomoJs\n", true));
$hashOfMatomoJsMinified = base64_encode(hash("$cspAlgo", "$outputMatomoJsMinified", true));
////////////////////////////////////////////////
$outputMatomoHml  = '';
$outputMatomoHml .= "<!-- Matomo - Embedding JS file after load event -->\n";
$outputMatomoHml .= "<!--          CSP hash: $cspAlgo-$hashOfMatomoJs -->\n";
$outputMatomoHml .= "<script>";
$outputMatomoHml .= "$matomoJs";
$outputMatomoHml .= "</script>\n";
$outputMatomoHml .= "<!-- END Matomo -->";
////////////////////////////////////////////////
$outputMatomoHmlMinified  = '';
$outputMatomoHmlMinified .= "<!-- Matomo - Embedding JS file after load event -->\n";
$outputMatomoHmlMinified .= "<!--          CSP hash: $cspAlgo-$hashOfMatomoJsMinified -->\n";
$outputMatomoHmlMinified .= "<script>";
$outputMatomoHmlMinified .= "$outputMatomoJsMinified";
$outputMatomoHmlMinified .= "</script>\n";
$outputMatomoHmlMinified .= "<!-- END Matomo -->";
///////////////////////////////////////
echo "\n\n------ Matomo JS loader ------------------------------------\n";
echo $outputMatomoJs;
echo "\n\n------ CSP hash for Matomo JS loader : $cspAlgo  ------------------------------------\n";
echo "$cspAlgo-$hashOfMatomoJs";
echo "\n\n------ Matomo HTML  ------------------------------------\n";
echo "$outputMatomoHml";
echo "\n\n------ CSP hash for Matomo JS loader minified : $cspAlgo  ------------------------------------\n";
echo "$cspAlgo-$hashOfMatomoJsMinified";
echo "\n\n------ Matomo HTML minified ------------------------------------\n";
echo "$outputMatomoHmlMinified";
echo "\n\n------------------------------------------\n";
echo "$cspAlgo-$hashOfMatomoJsMinified";
echo "\n\n------------------------------------------\n";



