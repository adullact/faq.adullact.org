# To update Git submodule of theme on main repository

```bash
cd faq.adullact.org/  # your project directory

# Update GIT submodule remote URL (mandatory if .gitmodules has updated)
git submodule sync --recursive

# Update GIT submodule working dir (fetch + check out)
git submodule update --init --recursive

# Update refs off GIT submodule on main repository
git submodule update --remote

# Commit changes
git commit -m "CHORE: update submodule ref"
```

We **strongly** recommend to follow the following examples
for the commit message and the merge request :

- [commit example](https://gitlab.adullact.net/adullact/faq.adullact.org/-/commit/2623374c462c403447d564e6738050df417789dc)
  - Add a link to old commit used for theme sub-module before update
  - Add a link to new commit used for theme sub-module after update
  - Add BREAKING CHANGE footer with a message for developers:
    > `BREAKING CHANGE: update Git submodule on your local repository`
- [Merge request example](https://gitlab.adullact.net/adullact/faq.adullact.org/-/merge_requests/146)
  - Add a link to old commit used for theme sub-module before update
  - Add a link to new commit used for theme sub-module after update
  - Add a message for developers : "To do : after the merge of the MR ..."
  - Add **breaking-change** label

This allows a quick verification of change made.

## To do after the update Git submodule of theme on main repository

After updating the theme's Git submodule on the main repository,
all developers must update the Git submodule on their local repository.

```bash
# Fetch changes
git remote update -p

# Update default branch from remote default branch
git checkout dev
git pull

# Update GIT submodule working dir (fetch + check out)
git submodule update --init --recursive
```
