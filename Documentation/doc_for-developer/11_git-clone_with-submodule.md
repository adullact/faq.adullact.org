# Clone repository and load GIT submodule

You should have already done [Prerequisites](10_prerequisites.md) step.

## Load GIT submodules

### During cloning process

```bash
git clone --recursive git@gitlab.adullact.net:adullact/faq.adullact.org.git
cd faq.adullact.org
```

### After cloning process

```bash
git clone git@gitlab.adullact.net:adullact/faq.adullact.org.git
cd faq.adullact.org
git submodule update --init --recursive
```
