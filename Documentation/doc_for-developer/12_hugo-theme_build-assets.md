# Install theme NPM packages and compile assets with GULP

You should have already done these steps:

- [Prerequisites](10_prerequisites.md)
- [Clone repository and load GIT submodule](11_git-clone_with-submodule.md)

```bash
cd faq.adullact.org/  # your project directory
cd .gitlab/website_hugo/themes/hugo-geekdoc-adullact/

# Install theme NPM packages
npm install --no-package-lock

# Compile theme assets with GULP
npx gulp default
```
