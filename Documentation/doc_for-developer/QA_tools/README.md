# QA - DEV

- [Oline tools for **PROD** website](QA_online-tools_for-PROD-website.md)
- [Oline tools for **DEV** website](QA_online-tools_for-DEV-website.md)

## QA - Old URLs of Joomla

see: [.htaccess](https://gitlab.adullact.net/adullact/faq.adullact.org/-/blob/dev/.gitlab/ci/build_additional_files/.htaccess)

### Crawl 2021.07.21 via Asqatasun

- [source files](../00_migration-Joomla-to-Hugo/crawl/2021.07.20_via-asqatasun-v4/)
- DEV website
  - [/QA_URLs/old-url-joomla_DEV_crawl-2021.07.20.html](https://faq-dev.adullact.org/QA_URLs/old-url-joomla_DEV_crawl-2021.07.20.html)
  - [Prefconfigured W3C LinkChecker : crawl Joomla 2021.07.20](https://validator.w3.org/checklink?uri=https%3A%2F%2Ffaq-dev.adullact.org%2FQA_URLs%2Fold-url-joomla_DEV_crawl-2021.07.20.html&hide_type=all&depth=&check=Check)
  - [Prefconfigured W3C LinkChecker : crawl Joomla 2021.10.07](https://validator.w3.org/checklink?uri=https%3A%2F%2Ffaq-dev.adullact.org%2FQA_URLs%2Fold-url-joomla_DEV_crawl-2021.10.07_HTTPS_links.html&hide_type=all&depth=&check=Check)
- PROD website
  - [/QA_URLs/old-url-joomla_PROD_crawl-2021.07.20.html](https://faq-dev.adullact.org/QA_URLs/old-url-joomla_PROD_crawl-2021.07.20.html)
  - [Prefconfigured W3C LinkChecker : crawl Joomla 2021.07.20](https://validator.w3.org/checklink?uri=https%3A%2F%2Ffaq-dev.adullact.org%2FQA_URLs%2Fold-url-joomla_PROD_crawl-2021.07.20.html&hide_type=all&depth=&check=Check)
  - [Prefconfigured W3C LinkChecker : crawl Joomla 2021.10.07](https://validator.w3.org/checklink?uri=https%3A%2F%2Ffaq-dev.adullact.org%2FQA_URLs%2Fold-url-joomla_PROD_crawl-2021.10.07_HTTPS_links.html&hide_type=all&depth=&check=Check)

## QA - Open-source softwares

- [W3C tools](https://w3c.github.io/developers/tools/#tools)
- Security
  - [Arachni](https://github.com/Arachni/arachni) (web application security scanner framework)
  - Content-Security-Policy (CSP)
    - [salvation](https://github.com/shapesecurity/salvation) (Java parser, warn about policy errors)
  - Mozilla Observatory
    - [CLI client for Mozilla Observatory](https://github.com/mozilla/observatory-cli)
    - [HTTP Observatory](https://github.com/mozilla/http-observatory) (local scanner : CLI and CI)
- Web accessibility
  - Asqatasun
    - [Asqatsun Docker image](https://hub.docker.com/r/asqatasun/asqatasun/)
    - [Install Asqatasun on a server](https://doc.asqatasun.org/en/10_Install_doc/Asqatasun/)
- Web perf
  - Webpagetest
  - [Yellowlab](https://github.com/gmetais/YellowLabTools/) (API, npm CLI, Grunt task, ...)
  - [Sitespeed.io](https://www.sitespeed.io/) (npm or docker is needed)
- Global tools
  - [Sonarwhal](https://github.com/sonarwhal/sonarwhal) (Node.js v8)
