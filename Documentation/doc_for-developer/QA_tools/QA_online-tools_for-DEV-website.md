# QA - Oline tools for DEV website

[faq-dev.adullact.org](https://faq-dev.adullact.org)

## URL

- [http](http://faq-dev.adullact.org)
- [https](https://faq-dev.adullact.org)
- Files:
  - [/humans.txt](https://faq-dev.adullact.org/humans.txt)
  - [/robots.txt](https://faq-dev.adullact.org/robots.txt)
  - [/.well-known/security.txt](https://faq-dev.adullact.org/.well-known/security.txt)

```bash
http://faq-dev.adullact.org
https://faq-dev.adullact.org
https://faq-dev.adullact.org/humans.txt
https://faq-dev.adullact.org/robots.txt
https://faq-dev.adullact.org/.well-known/security.txt
```

## QA - Online tools

`*` preconfigured tools

- HTTP Response :
  - [httpstatus.io](https://httpstatus.io/)
  - [URLitor - HTTP Status & Redirect Checker](http://www.urlitor.com/)
  - [HTTP Response Checker](https://www.webmoves.net/tools/responsechecker)
  - [Server Headers](http://tools.seobook.com/server-header-checker/?url=https%3A%2F%2Ffaq-dev.adullact.org%0D%0Ahttps%3A%2F%2Fwww.faq-dev.adullact.org%0D%0Ahttp%3A%2F%2Fwww.faq-dev.adullact.org%0D%0Ahttp%3A%2F%2Ffaq-dev.adullact.org%0D%0A&useragent=11&protocol=11) `*`
- Security
  - [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
  - [Mozilla Observatory](https://observatory.mozilla.org/analyze/faq-dev.adullact.org) `*` (HTTP header, SSL, cookies, ...)
  - [Security Headers](https://securityheaders.io/?q=https://faq-dev.adullact.org) `*` (HTTP header)
  - Content-Security-Policy (CSP)
    - [cspvalidator.org](https://cspvalidator.org/#url=https://faq-dev.adullact.org) `*`
    - [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://faq-dev.adullact.org) `*`
  - SSL
    - [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=faq-dev.adullact.org) `*`
    - [tls.imirhil.fr](https://tls.imirhil.fr/https/faq-dev.adullact.org) `*`
  - DNS
    - [DNSViz](http://dnsviz.net/d/faq-dev.adullact.org/dnssec/) `*` (DNSSEC)
    - [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/faq-dev.adullact.org) `*`   (DNSSEC)
    - [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
- W3C tools
  - [HTML validator](https://validator.w3.org/nu/?doc=https://faq-dev.adullact.org&showsource=yes&showoutline=yes&showimagereport=yes) `*`
  - [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://faq-dev.adullact.org&profile=css3) `*`
  - [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://faq-dev.adullact.org) `*`
  - [Link checker](https://validator.w3.org/checklink?uri=https://faq-dev.adullact.org&hide_type=all&depth=&check=Check) `*`
- Web accessibility
  - [Asqatasun](https://app.asqatasun.org)
- Web perf
  - [Yellowlab](http://yellowlab.tools)
  - [Webpagetest](https://www.webpagetest.org/)
  - [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://faq-dev.adullact.org) `*`
- HTTP/2
  - [Http2.pro](https://http2.pro/check?url=https://faq-dev.adullact.org) `*` (check server HTTP/2, ALPN, and Server-push support)
- Global tools (webperf, accessibility, security, ...)
  - [Dareboost](https://www.dareboost.com)  (free trial)
  - [Sonarwhal](https://sonarwhal.com/scanner/)

------

- Social networks
  - [Twitter card validator](https://cards-dev.twitter.com/validator)
- structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  - [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://faq-dev.adullact.org/)  `*`
  - [Structured Data Linter](http://linter.structured-data.org/?url=https://faq-dev.adullact.org)  `*`
  - [Microdata Parser](https://www.webmoves.net/tools/microdata)
- Google image
  - [images used on the website](https://www.google.fr/search?tbm=isch&q=site:faq-dev.adullact.org)  `*`  (site:faq-dev.adullact.org)
  - [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:faq-dev.adullact.org+-src:faq-dev.adullact.org) `*`  (site:faq-dev.adullact.org -src:faq-dev.adullact.org)
  - [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:faq-dev.adullact.org)  `*`    (src:faq-dev.adullact.org)
  - [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:faq-dev.adullact.org+-site:faq-dev.adullact.org)  `*`   (src:faq-dev.adullact.org -site:faq-dev.adullact.org)

