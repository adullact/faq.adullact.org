+++
title = "Acheter un logiciel libre ?"
description = "Acheter un logiciel libre ?"
tags = [ "Logiciel libre" ]
date = "2009-11-04"
+++
{{< toc >}}

## La mutualisation est un instrument d'assurance

Il est utile de rappeler que la mutualisation est un instrument d'assurance. Il s'agit de se prémunir ensemble contre
des risques qui menacent chacun.

### Risques en matière de logiciel contre lesquels se prémunir avec du logiciel libre

Deux risques importants qui menacent les systèmes d'information en matière de logiciel sont : la situation de client
captif (vendor lock in) et la dépendance à des standards propriétaires (coût de sortie élevés). Le recours au logiciel
libre permet :

1. de séparer le choix de la solution et le choix de la prestation (et donc aiguise la concurrence)
2. d'augmenter l'interopérabilité.

### Qu'il est déraisonnable de vouloir se protéger seul

Il est possible de répondre seul à ces risques, mais cela coûte cher. Il est par exemple illusoire de vouloir mettre en
concurrence le libre et le propriétaire sur le même cahier des charges s'il n'existe pas de solution libre déjà
développée et approchant de la demande.

## Cas 1 : la solution existe en libre

### L'heure du choix

Lorsqu'il existe des solutions libres qui répondent peu ou prou au besoin, il n'y a pas besoin d'appel d'offre pour
faire un choix (le code des marchés publics ne s'adresse qu'aux objets onéreux). Il est possible :

1. de se faire assister pour faire un choix
2. de recourir à des ressources internes pour ce faire.

Mettre ensuite en concurrence1 sur la garantie, les prestations, la formation, ...

## Cas 2 : il n'existe pas de solution en libre

### Phase 1 - Groupe de définition

Il faut chercher qui pourrait avoir des besoins analogues.

Ceci permettra de distinguer dans nos besoins :

* Le générique
* Le spécifique

On veillera particulièrement à trois principes :

1. L'architecture modulaire
2. Le respect scrupuleux des standards
3. L'utilisation de composants pérennes, avec si possible un travail conjoint avec leurs communautés (certaines SSLL
  ont des droits en écriture dans les "trunks" de certains logiciels et participent à leur développement).

### Phase 2 - Achat groupé du générique

Le code des marchés publics permet les marchés groupés. Ils sont particulièrement adaptés pour acheter du développement
logiciel à plusieurs. Mais beaucoup de collectivités hésitent à y recourir en arguant de la lourdeur administrative.
Est-il possible, si l'on se fait confiance (par convention ?), d'acheter séparément des briques logicielles libres et
de les assembler au terme des marchés ?

Il est utile d'alotir le développement et de mettre plusieurs entreprises sur le développement. En demandant si
possible l'utilisation de méthodes agiles. Cela évite la viscosité que peut ajouter le premier prestataire, qui a
tendance à se croire (et à être) ensuite en terrain conquis. Cela accélère la concurrence sur le service car il est
probable que toutes les entreprises participant au développement feront par la suite du service sur le produit.

### Penser à la suite

La suite de la vie d'un logiciel libre métier, ce sont des marchés de services (le développement de fonctionnalités
supplémentaires est un service), dont il faudra arbitrer le dépôt de code associé dans le projet.

Ce rôle d'administrateur du projet peut faire partie du marché initial.

Pour qu'il y ait concurrence, il faut qu'il y ait plusieurs prestataires capables de répondre.

## Où trouver des logiciels libres métiers ?

Le comptoir du Libre est une plate-forme collaborative, recensant les logiciels libres métiers utiles aux services publics, à leurs utilisateurs, ainsi qu'à leurs prestataires.

### Pourquoi se rendre sur Le comptoir du libre ?

Cette plate-forme permet non seulement de trouver les outils correspondant aux besoins de chacun, mais aussi de partager des expériences et d'identifier des partenaires.
Les notes et les avis attribués par des utilisateurs ayant déjà eu recours à ces outils, sont consultables sur la plate-forme.

### Comment accéder à la plate-forme ?

* [Site web **Le Comptoir du Libre**](https://comptoir-du-libre.org/fr/).
* Consulter l'article dédié sur la FAQ pour faire ses [premiers pas sur Le Comptoir du Libre](../../logiciels/comptoir-du-libre/decouvrir-le-comptoir-du-libre/)
  tout en étant guidé.
