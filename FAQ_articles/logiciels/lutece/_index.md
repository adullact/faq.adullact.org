+++
title = "Logiciel Lutèce"
description = "Présentation de LUTECE"
tags = [ "Lutèce", "Mairie de Paris" ]
date = "2011-12-30"
+++
{{< toc >}}

## Histoire

Lutèce a vu le jour en 2002, développé par la Mairie de Paris. Sa création est partie d’un besoin de mettre à
disposition des mairies d'arrondissement de Paris, un outil de type CMS pour gérer leurs propres sites et confier la
gestion à des webmestres qui n'avaient pas forcément des compétences précises dans ce domaine.

Il devait permettre un fonctionnement en réseau, pour que les informations publiées par les services de la capitale
soient accessibles aux Parisiens. Dernier impératif, son architecture devait être bâtie sur des technologies libres.

## Description de l'outil

Lutèce est une plate-forme de co-publication sur Internet, donc un CMS (Content Management System, ou système de
gestion de contenu) développé par la direction informatique de la Mairie de Paris, qui permet de créer rapidement
un site internet ou intranet dynamique basé sur du contenu HTML, XML... .

Lutèce fournit une interface d'administration simple qui peut être utilisée par des utilisateurs sans compétences
techniques particulières. La palette de services offerts sur un site peut être facilement personnalisée, il suffit
d'ajouter un ou plusieurs plugins pour accéder à de nouvelles fonctionnalités.

## Open source

### Site officiel

[https://lutece.paris.fr/lutece/](https://lutece.paris.fr/lutece/)

### Forge

[github.com/lutece-platform](https://github.com/lutece-platform)
