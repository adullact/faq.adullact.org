+++
title = "Logiciels libres"
description = "Présentation des logiciels libres métiers"
tags = [ "Comptoir du Libre", "Logiciel libre" ]
date = "2018-10-02"
geekdocCollapseSection = true
+++
{{< toc >}}

Dans sa mission de promotion des logiciels libres pour les métiers des collectivités territoriales nous mettons à disposition le comptoir du libre.
C'est une plateforme collaborative pour trouver un logiciel, des utilisateurs et des prestataires.

## Le comptoir du libre

Si vous recherchez un logiciel libre, utile dans les métiers des collectivités, vous pouvez aussi consulter notre
[Comptoir du Libre](https://comptoir-du-libre.org/fr/). Identifier des logiciels libres métier n'est que la première
des fonctionnalités du comptoir.

Une fois votre(vos) logiciel(s) identifié(s), via la plateforme, vous pouvez entrer en contact avec les collectivités
qui se sont déclarées utilisatrices. Cela permet de discuter entre collègues de différentes collectivités. Ceci pour,
par exemple, disposer d'informations de terrain quant au comportement d'un logiciel identifié.

Et enfin, une fois que vous avez trouvez le logiciel qui répond à vos besoins, le comptoir permet de connaître les
prestataires de services pour accompagner la collectivité à la mise en œuvre, au support ou à la formation.

Vous pourrez, vous aussi, participer à cette chaîne collaborative en vous déclarant collectivité utilisatrice ou
rédiger des avis à propos des logiciels libre que vous utilisez dans votre collectivité.

Autres articles intéressants :

* [Acheter un logiciel libre ?](../general/acheter-un-logiciel-libre/)
* [FAQ ADULLACT](https://faq.adullact.org/ "faq")

____
{{< toc-tree >}}
