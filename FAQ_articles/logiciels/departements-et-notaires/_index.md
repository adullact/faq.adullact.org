+++
title = "Départements et Notaires"
description = "Logiciel Départements et Notaires"
tags = ["Logiciel libre", "Premiers pas", "Départements et Notaires"]
date = "2024-10-30"
+++

![Départements et Notaires - Logo](/images/Logiciels/departements-et-notaires/logo-departements-et-notaires.png)

L’application Départements et Notaires permet aux départements d’apporter une réponse en temps réel aux études
notariales chargées d’une succession et s’interrogeant sur l’existence éventuelle d’une créance du département
au titre de l’aide sociale.

Le site dédié **[departements-et-notaires.adullact.org](https://departements-et-notaires.adullact.org/)** présente le
logiciel, son histoire, l'écosystème communautaire, et une documentation fonctionnelle.

Autres ressources :

* La [fiche Départements et Notaires sur le Comptoir du Libre](https://comptoir-du-libre.org/fr/softwares/115)
  donne en particulier accès aux collectivités qui utilisent le logiciel
* Le [code source sur le GitLab de l'ADULLACT](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2),
  avec une licence libre [AGPL v3](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/LICENSE.txt)
* Les [comptes rendus des GTC](https://adullact.org/departements-et-notaires) (Groupes de Travail Collaboratif)
* La liste de diffusion mail `dep-notaires@listes.adullact.org` ([nous contacter](https://adullact.org/contact) pour y être ajouté)
