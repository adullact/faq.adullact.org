+++
title = "Critères d'ajout d'un logiciel dans Comptoir du libre"
description = "Les critères à prendre en compte pour l'ajout d'un logiciel sur le Comptoir du libre"
tags = [ "Comptoir du Libre", "OSI", "Logiciel Libre" ]
date = "2016-11-04"
+++
{{< toc >}}

## Code source

Le code source du logiciel doit être publié :

- sous l'une des [licences libres](https://opensource.org/licenses) approuvées par l'OSI.
- sans délai entre la publication du code source et celle des fichiers exécutables.
- dans un dépôt public sans authentification.

## Formulaire d'ajout du logiciel

Concernant le champ "URL du dépôt" :

- Ce champ est obligatoire.
- L'URL à fournir est celle du dépôt consultable publiquement.
- Cette URL doit correspondre au code source du logiciel.
- Cette URL doit permettre de faire un `git clone` (ou une commande équivalente pour d'autre gestionnaire de code source).
