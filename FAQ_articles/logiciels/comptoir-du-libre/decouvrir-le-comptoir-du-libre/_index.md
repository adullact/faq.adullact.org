+++
title = "Premiers pas sur le Comptoir du libre"
description = "Le Comptoir du Libre facilite l'adoption des logiciel libres métier au sein des collectivités et administrations."
tags = [ "Assistance", "Comptoir du Libre", "Premiers pas" ]
date = "2016-11-04"
+++
{{< toc >}}

(dernière mise à jour : 04/11/2016)

Le Comptoir du Libre facilite l'adoption des logiciels libres métiers au sein des collectivités et administrations.

## Grandes idées

Le [Comptoir du Libre](https://comptoir-du-libre.org/fr/) est une application web présentant les entités suivantes :

* Collectivité
* Logiciel Libre
* Prestataire

Une Collectivité se déclare utilisatrice d'un Logiciel Libre, elle peut aussi le noter et laisser un témoignage d'utilisation.

Une entreprise peut se déclarer comme Prestataire sur un Logiciel Libre.

Ainsi toute nouvelle collectivité intéressée par un logiciel libre donné verra :

* les autres collectivités utilisant déjà le logiciel (et pourra si elle le souhaite les contacter),
* les prestataires existant autour de ce logiciel.

## Droits et permissions

Une collectivité peut :

* Ajouter un logiciel.
* Se déclarer utilisatrice d'un logiciel.
* Noter et commenter un logiciel.
* Se déclarer prestataire sur un logiciel.

Une entreprise peut :

* Ajouter un logiciel.
* Se déclarer prestataire sur un logiciel.

## Ajouter un logiciel

Les informations requises pour ajouter un logiciel sont :

* Nom
* URL du dépôt des sources (URL sur laquelle on peut faire un git clone ou un svn co)
* Logo
* (optionnel) Description

## Contact

Matthieu Faure, chef de projet Comptoir du Libre :

* [comptoir@adullact.org](mailto:comptoir@adullact.org)
* 04 67 65 05 88
