+++
title = "Quel CMS choisir ?"
description = ""
tags = [ "CMS", "Joomla", "Wordpress", "Spip", "Typo3", "Application web", ]
date = "2005-10-04"
+++
{{< toc >}}

Il existe actuellement des centaines de CMS disponibles sur le marché (libres ou non), quelle solution choisir ?

Quelques exemples, et une méthode très simplifiée pour vous aider à choisir votre solution.

## 1\. Vous avez besoin d'un outil simple, offrant une prise en main rapide pour créer un site web orienté blog ou magazine

* [Wordpress](https://wordpress.org/), sans doute le plus connu des CMS grand public. Lancé en 2003, Wordpress a
  rapidement acquis une belle notoriété ; simple d'accès, doté de nombreuses fonctionnalités et d'une interface claire,
  Wordpress bénéficie en outre d'une grande communauté active lui permettant de bénéficier de nombreux plugins.
  WordPress inclut la gestion des liens externes, des rétroliens (trackbacks), et un système de gestion fine des
  commentaires. À noter que Wordpress dispose d'un service de publication en ligne (gratuit),
  [wordpress.com](https://wordpress.com/fr/), qui limite certaines fonctionnalités mais permet de publier et de
  référencer rapidement son blog.
* [Spip](https://www.spip.net/fr), une valeur sûre.

## 2\. Vous voulez mettre en place une véritable infrastructure de développement, orientée gestion de contenu

* [Mambo](https://www.mamboserver.com/), ou [Joomla](https://www.joomla.org/), tous deux créés par la même équipe. Mambo
  a été developpé par la société MIRO. Mais depuis l'annonce d'un projet de création d'une fondation supervisant le
  projet Mambo, et de peur que ce projet ne perde son aspect libre et opensource, de nombreux développeurs ont décidé
  de prendre du recul et de créer un fork de Mambo nommé Joomla!
* [Xoops](https://www.frxoops.org/) est un CMS orienté objet dérivé de PHPNuke et écrit en PHP.
* [Typo3](https://typo3.org/), technologie PHP/MySQL. Typo3 (V.3.6) est multisites et présente une très forte modularité
  lui permettant de couvrir tous vos besoins ou presque. Il offre en outre la possibilité à chacun de le personnaliser
  à son goût... via les "extensions". Typo3 possède plus de 1200 extensions.
* [eZ publish](https://developers.ibexa.co/ez-platform), technologie PHP/MySQL. Avec eZ Publish on peut réaliser un site de manière
  personnalisée : on peut tout faire ! Il y a par exemple des outils pour la création de menus (menu simple en haut ou
  à gauche, menu double en haut, ou à gauche et en haut...) et de toolbars. Un des principaux avantages de ezPublish
  est sa capacité à se programmer sur un mode objet. Les avantages :
  * génère du code HTML standard W3C
  * dispose d'une véritable communauté
  * capacité d'import/export des contenus du site
  * workflow simple et éditable ; possibilité de notification par mail ; archivage automatique d'anciens articles
  * backoffice entièrement editable selon un modèle objet. Exemple: créer un nouveau type d'article, héritant de
    l'article standard mais où on aura spécifié que le titre est obligatoire; le chapo optionnel et le corps de
    l'article n'est pas multilangue. Il y a plusieurs dizaines de classes d'objets par défaut, toutes éditables.
  * utilisation de gabarit pour mieux séparer le contenu de la forme
  * facilité d'hébergement (PHP)

Un exemple labellisé accessiweb réalisé avec ezPublish: le
[planétarium de Montpellier.](http://planetarium-galilee.montpellier-agglo.com/)

## 3\. Vous voulez mettre en place un portail orienté travail collaboratif

* [Plone](https://plone.org/), technologie Python. Plone permet de construire facilement des portails collaboratifs,
  efficaces et très propres. Il est écrit en Python ce qui permet aux développeurs d'étendre facilement ses
  fonctionnalités ou d'en modifier le code. Pour en savoir plus...
* [Nuxeo CPS](https://www.nuxeo.com/content-services-platform/) est un progiciel libre d'ECM (Enterprise
  Content Management). CPS Platform, qui fonctionne sur le serveur d'applications Zope, est actuellement, selon
  plusieurs études publiées par le Gartner Group, KnowledgeConsult ou Markess International, le progiciel libre le
  plus complet de son domaine, toutes technologies confondues.

**Quelques sites pour vous permettre d'approfondir le sujet :**

L'article [Systèmes de Gestion de Contenu](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_contenu)
sur Wikipedia.

La rubrique [CMS](https://framalibre.org/tags/cms) de Framasoft.
