+++
title = "Solution d'anti-virus libre"
description = ""
tags = [  ]
+++
{{< toc >}}

"L'antivirus est le degré zéro de la sécurité informatique..."

Les virus informatiques (comme les virus biologiques) se renouvellent, changent régulièrement d'aspect, et se propagent
par différents moyens (réseaux, internet, DVD, clef USB, etc.). Il est donc essentiel d'avoir un antivirus installé, et
surtout, mis à jour.

L'antivirus est, en règle générale, constitué d'un programme toujours en mémoire de l'ordinateur, et vérifiant sans
cesse la présence de "signatures virales" ou de séquences précises d'octets qui trahissent la présence d'un virus connu.

## Quelle solution d'anti-virus libre ?

### ClamAV

[ClamAV](https://www.clamav.net/) est disponible pour les systèmes Linux et MSWindows (ClamWIN), léger et
performant, l’antivirus ClamAV est une solution libre antivirale (GPL). Vous trouverez toutes les informations sur
ClamAV sur le site officiel. [https://www.clamav.net](https://www.clamav.net/)

L'installation de clamAV se fait trés simplement sur Linux car il est prévu dans la plupart des ditributions.

* Pour Mandriva : `# urpmi clamav`
* Pour Fedora core : `# yum install clamav`
* Pour Debian : `# apt-get install clamav`

La commande clamscan permet de scanner les dossiers de la machine, la commande freshclam permet de réaliser les mises à
jour , ces deux commandes peuvent facilement être automatisées avec cron. Nous allons détailler le fonctionnement de
ces deux commandes à travers quelques exemples.

* Scanner le disque dur entier = `clamscan -r /`
* Scanner tous les fichiers du dossier personnel = `clamscan -r /home/utilisateur`
* Scanner le disque dur entier et émettre un son une fois un fichier infecté trouvé = `clamscan -r –bell –mbox -i /`
* Affiche uniquement les fichiers infectés = `–infected (-i)`
* Emet un son lors de la détection d’un virus = `–bell`
* Scanne les sous-dossiers récursivement = `–recursive (-r)`

La mise à jour des définitions anti-virus se fait à l'aide d'une autre commande : `freshclam` ; également disponible
sous forme de démon: `freshclamd`

**En savoir plus :**

Voir l'[article wiki](https://debian-facile.org/doc:systeme:clamav) sur le site Debian-Facile

Voir le [test d'efficacité des anti-virus Linux](https://linuxfr.org/news/tests-defficacite-des-antivirus-linux)
sur le site LinuxFR
