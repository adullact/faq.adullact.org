+++
title = "Vers quelle licence se tourner ?"
description = "Vers quelle licence se tourner ?"
tags = [ "Juridique", "Licence libre" ]
date = "2014-02-20"
+++
{{< toc >}}

Cet article est une introduction synthétique aux différentes licences libres. Celles-ci peuvent être classées par
niveau de permissivité :

**Les licences non permissives ou dites à fort copyleft** (contaminantes) imposent de distribuer le
logiciel modifié ou non sous la même licence ou sous une licence dite "expressément compatible". L'obligation de
distribution du code source modifié ne vaut qu'à partir de la distribution de ce logiciel.
(Licence [GPLv3](../../presentation-licences/gpl-v3/),
licence [AGPL v3](../../presentation-licences/agpl-v3/),
licence [CeCILL v2](../../presentation-licences/cecill-v2/).

**Les licences non permissives en matière de dérivation, mais permissives en matière de composition** permettent
d'assembler les composants du logiciel sous une licence A avec ceux d'un logiciel sous une licence B et de distribuer
le résultat sous la licence B. Cependant lorsque le code source est simplement modifié, sans ajout de composants soumis
à une autre licence, le code modifié doit être redistribué sous cette même licence A (Licence LGPLv3).

**Les licences permissives en matière de composition et dérivation** permettent de modifier le logiciel comme le veut
l'utilisateur, sans restriction, puis de distribuer ce logiciel modifié sous une licence de son choix, même
propriétaire (Licence BSD).

Il y a également toute une série d'[articles juridiques](../../presentation-licences/) décrivant plusieurs
licences dans le détail:

* [Cecill V2](../../presentation-licences/cecill-v2/)
* [Cecill V2.1](../../presentation-licences/cecill-v2.1/)
* [GPL V3](../../presentation-licences/gpl-v3/)
* [AGPL V3](../../presentation-licences/agpl-v3/)
* [EUPL V1.1](../../presentation-licences/eupl-v1.2/)

Voir aussi l'[article de l'AFUL](https://aful.org/ressources/licences-libres).

Voir aussi le [décret N°2017-638 du 27 avril 2017.](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000034502557)
