+++
title = "Certificats électroniques et dispositifs de télétransmission"
description = "Tout ce que vous avez toujours voulu savoir sur les certificats électroniques et les tiers de télétransmission (TDT) ou de la nécessité d'acquérir un certificat compatible RGS"
tags = [ "S²LOW", "S2LOW", "Certificat", "Certificat RGS", "PASTELL" ]
date = "2007-04-12"
+++
{{< toc >}}


Tout ce que vous avez toujours voulu savoir sur les certificats électroniques et les tiers de télétransmission (TDT).
ou de la nécessité d'acquérir un certificat compatible RGS\*\*.

## Le cahier des charges pour les tiers de télétransmission

L'association [ADULLACT](https://adullact.org/) s'est penchée sur ce sujet afin d'aider ses adhérents (et toutes
les collectivités) à s'y retrouver.

L'ADULLACT a étudié plusieurs options pour permettre aux collectivités de diminuer le coût à investir afin de permettre
à leurs agents de télétransmettre vers @CTES et HELIOS :

* les agents télétransmetteurs peuvent-ils se connecter sur un TDT avec un certificat logiciel ?
* la collectivité est-elle autorisée à fabriquer elle-même les certificats pour ses agents (hors AED) ?
* l'opérateur du dispositif de télétransmission est-il habilité à fabriquer lui même des certificats dédiés à son
  dispositif, pour les collectivités utilisant son dispositif ?
* existe-t-il une solution gratuite pour obtenir des certificats certifiés ?

Pour répondre à ces questions, l'ADULLACT a rencontré les responsables de
[@CTES](https://www.collectivites-locales.gouv.fr/institutions/ctes-dematerialisation-de-la-transmission-des-actes) au MIAT et à la DGCL. Après
concertations avec d'autres acteurs officiels (MINEFI et Helios, DGME...), la réponse du MIAT a été très claire :
"*non*" à toutes les questions !

Et de rappeler que TOUS les opérateurs offrant une plateforme habilitée à télétransmettre avec le ministère se sont
engagés contractuellement sur un protocole d'authentification très précis. Ce protocole est abordé dans le
[cahier des charges du MIAT](https://www.collectivites-locales.gouv.fr/sites/default/files/Institution/6.%20contr%C3%B4le%20de%20l%C3%A9galit%C3%A9/cahier_des_charges_actes.pdf),
puis confirmé et précisé dans la convention-type de raccordement que chaque opérateur signe avec le MIAT.

## Deux cas possibles (version simplifiée)

Soit l’agent d’une collectivité accède au dispositif de télétransmission au moyen d’un portail web, l'agent doit disposer d'un certificat RGS\*\*.

Soit l'agent d'une collectivité accède au dispositif de télétransmission au moyen d'un outil métier, alors contactez votre fournisseur de l'outil métier.

## Deux cas possibles (version détaillée)

Soit l’agent d’une collectivité accède au dispositif de télétransmission au moyen d’un portail web. Le cas échéant, l’exigence
SEC-01 s’applique : un certificat d’identification personnel est exigé de l’agent lors de l’accès à la
fonction de transmission.

Soit l’agent accède à la fonction de télétransmission par l’intermédiaire d’une application métier ou d’un serveur
auquel il accède par un réseau interne à la collectivité. Auquel cas, le dispositif de télétransmission
exige une authentification au moyen d’un certificat serveur conformément à l’exigence SEC-02.

Extrait du cahier des charges dans sa partie 2.c "*Sécurisation des échanges avec l'application @CTES*" :

### Exigence SEC-01

> Pour l’authentification d’un agent d’une collectivité lors de l’accès à la fonction
> de transmission, le dispositif doit exiger l’usage exclusif d’un moyen d’identification personnel, d’un
> degré de sécurité au moins égal au niveau substantiel du référentiel général de sécurité. Le dispositif
> doit vérifier le niveau de sécurité du moyen d’identification présenté lors de l’enrôlement de l’agent,
> ainsi que sa bonne adéquation à l’usage prévu.

### Exigence SEC-02

> Exigence SEC-02 : Pour l’authentification d’une application métier ou d’un serveur d’une collectivité
> lors des échanges avec le dispositif de télétransmission, il est exigé l’usage exclusif d’un moyen
> d’identification de serveur d’un degré de sécurité au moins égal au niveau élémentaire du référentiel
> général de sécurité. Le dispositif doit vérifier le niveau de sécurité du moyen d’identification présenté
> lors de l’enrôlement de la collectivité, ainsi que sa bonne adéquation à l’usage prévu.

## Pour en savoir plus

[@CTES - Dématérialisation de la transmission des actes](https://www.collectivites-locales.gouv.fr/institutions/ctes-dematerialisation-de-la-transmission-des-actes "@ctes")

[Page de l'organisme d'homologation des autorités de certification](https://www.lsti-certification.fr/fr/psce/) contenant un lien vers une liste d'autorités de certification homologuées.

Voir aussi: [Certificat RGS\*\* - le cas des multi-mandats](../../../juridique/certificat-rgs-cas-multi-mandats/)
