+++
title = "Comment accéder à mes flux ACTES convertis en flux SEDA"
description = "Comment accéder à mes flux ACTES convertis en flux SEDA"
tags = [ "S²LOW", "S2LOW", "as@lae", "ACTES", "Flux SEDA" ]
type = "blog"
+++
{{< toc >}}

Il s'agit de la conversion de flux présents dans le TDT mutualisé S²LOW ADULLACT, conformément au Standard d'Échange
de Données pour l'Archivage (SEDA). Ceci pour un versement vers un système d’archivage électronique (SAE) agréé à des
fins de consultation et d'archivage à valeur probante.

Pour mémoire le SEDA spécifie les transferts d'archives électroniques entre
les services producteurs des collectivités et les services de gestion des archives publiques.
Il est décrit dans le Référentiel général d'interopérabilité (RGI), et préconisé par le Service
interministériel des Archives de France (SIAF).

## Préambule

Certaines informations de configuration sont à renseigner par l'Administrateur S2low de la collectivité.
Tant que cette configuration n'est pas faite, le bouton "versement manuel" ne sera pas opérationnel. Ce bouton
"versement manuel" apparaît sur S²LOW, dans le "détail d'un acte", en bas de page, 2 mois après acquittement de l'acte.

## Utilisation

Une fois la configuration initiale faite, le bouton "versement manuel" apparaît en bas de page S²LOW de "détail d'un
acte", pour tous les actes acquittés depuis plus de 2 mois. Une série d'actions et d'effets se succèdent à l'occasion
du dialogue entre les espaces de télé-transmission et d'archivage  :

* L'agent souhaitant convertir et versé son flux ACTES dans le SAE dédié clique sur "versement  manuel".

* L'acte est envoyé pour conversion et transfert vers le SAE. Le statut de l'acte est mis à jour du côté
  S²LOW: "envoyé au SAE".

![Flux ACTES SEDA 1](/images/Logiciels/S2LOW/Flux-ACTES-SEDA1.png)

* Le SAE accuse réception de l'acte: "reçu par le SAE".

* Le flux est alors accessible dans le SAE, sous la forme d'un fichier SEDA appelé "Entrée", conformément à la
  sémantique archivistique. Dans les SAE il existe des fonctions de navigation et des filtres permettent de retrouver aisément
  le/les entrées archivées.

![Flux ACTES SEDA 2](/images/Logiciels/S2LOW/Flux-ACTES-SEDA2.jpg)

* Une fois l'acte versé dans le SAE, le statut est alors mise à jour du coté S²LOW: "archivé par le SAE".

* Un lien (URL) est mis à jour du côté S²LOW pour retrouver l'acte versé dans le SAE.

* Dans la foulée, le fichier principal de l'acte et ses pièces jointes sont effacées du côté S²LOW. Le statut est mis à jour :

![Flux ACTES SEDA 3](/images/Logiciels/S2LOW/Flux-ACTES-SEDA3.jpg)

* Le lien installé du côté S²LOW permet de récupérer directement l'acte dans l'environnement de stockage SEDA.

![Flux ACTES SEDA 4](/images/Logiciels/S2LOW/Flux-ACTES-SEDA4.jpg)

* L'acte complet (document principal, pièces jointes, acquittement préfecture) est accessible dans l'espace sous forme
  d'une archive normée (SEDA).
