+++
title = "Prérequis pour la mise en production de S²LOW (ACTES)"
description = "Prérequis pour la mise en production de S²LOW (ACTES)"
tags = [ "S²LOW", "S2LOW", "ACTES", "Assistance", "Premiers pas", "Pré-requis" ]
date = "2007-03-01"
+++
{{< toc >}}

Mode opératoire de démarrage et pré-requis demandés aux utilisateurs du module ACTES de la plate-forme nationale
mutualisée [S²LOW de l'Adullact](https://www.s2low.org).

## Pré-requis avant d'accéder à S²LOW

Avant de pouvoir se connecter [S²LOW de l'Adullact](https://www.s2low.org), votre organisme doit renseigner
le [formulaire de demande d'accès au service S2LOW](https://demarches.adullact.org/commencer/inscription-au-service-s2low-de-l-adullact).

Pour remplir cette demande d'accès, vous aurez besoin d'une convention signée avec la Préfecture et d'une adresse mail professionnelle.

### Convention avec la Préfecture

La [convention type](https://www.collectivites-locales.gouv.fr/files/files/dgcl_v2/actes/convention-type_transmission_actes_v11.odt
"convention type") requiert certaines informations précises (article 2):

* Nom du dispositif : **S2LOW**.
* Date de 1ere homologation **22 janvier 2007** (dernière ré-homologation : 23 décembre 2024).
* Nom de l'opérateur : **ADULLACT** (Association des Développeurs
  et Utilisateurs de Logiciels Libres pour les Administrations et les Collectivités Territoriales).
* Date du marché : les adhérents de l'association sont invités à renseigner
  ici la date de leur 1ere adhésion à l'association, renouvelée annuellement.

### Adresse mail professionnelle

Nous n'acceptons de créer un compte administrateur que sur des adresses mail professionnelles.
Pour être considérée telle quelle deux cas sont possibles :

* Votre organisme "*Ma Ville*" dispose d'adresses mails avec son propre nom de domaine, par exemple `contact@maville.fr`, nous la considérons defacto comme une adresse professionelle.
* Votre organisme "*Ma Ville*" utilise une adresse issue d'un fournisseurs de mail généraliste, par exemple `mairie.maville@orange.fr`,
  alors cette adresse doit être vérifiable par l'Adullact. Par exemple, nous pouvons vérifier que cette adresse mail est référencée sur le site web public de la mairie
  ou bien l'adresse mail est référencée sur [l'annuaire Service-Public](https://lannuaire.service-public.fr).

### Les postes utilisateurs

[S²LOW de l'Adullact](https://www.s2low.org) nécessite l'utilisation exclusive d'un navigateur WEB. L'ADULLACT préconise les navigateurs conformes W3C
comme Firefox par exemple.

## Quand pourrais-je télétransmettre ?

Suite à l'instruction du [formulaire de demande d'accès au service S2LOW](https://demarches.adullact.org/commencer/inscription-au-service-s2low-de-l-adullact),
nous allons créer pour votre organisme un compte administrateur S2low ainsi qu'un compte administrateur de notre [infrastucture de gestion de certificats](https://pki.adullact.org), alias IGC (en anglais PKI).

Ce compte administrateur S²LOW est basé sur un certificat logiciel issue de notre infrastructure de gestion de certificats.
Ce compte administrateur est donc habilité à administrer votre structure dans S²LOW. Mais, son certificat logiciel **NE PERMET PAS DE TÉLÉTRANSMETTRE**.

Avec le compte administrateur S²LOW, vous serez par exemple en mesure :

* de gérer tous les comptes de votre organisme (administrateur ou télétransmetteur). Un compte télétransmetteur doit être créé sur la base d'un certificat RGS\*\* que vous devez acquérir auprès d'une autorité de certification homologuée(\*).
* d'être autonome pour remplacer le certificat avec son expiration.
* de mettre à jour la classification.

Avec le compte administrateur [PKI](https://pki.adullact.org), vous serez en mesure de créer des certificats logiciels pour remplacer celui actuel qui va périmer.

## Assistance et formation

Vous pouvez contractualiser de l'assistance ou de la formation à propos de S2low auprès d'une entreprise partenaire de l'association.
Sur notre Comptoir du libre, vous pouvez trouvez [des prestataire à propos de S2LOW](https://comptoir-du-libre.org/fr/softwares/servicesProviders/5).

## Pour les certificats

Les certificats X.509 matériel pour les agents appelés à télétransmettre sont délivrés par une autorité de certification homologuées (\*).
Seuls les certificats de types RGS\*\* ou RGS\*\*\* (c'est à dire matériel USB, carte à puce...) sont acceptables pour télétransmettre via un tier de télétransmission.

Plus d'informations sur les certificats électroniques: [Certificat RGS\*\* : le cas des multi-mandats,](../../../juridique/certificat-rgs-cas-multi-mandats/) [Certificats électroniques et
dispositifs de télétransmission](../certificats-electroniques-et-dispositifs-teletransmission/)

## Plus d'informations

Plus d'information sur la [page du ministère de l'intérieur](https://www.collectivites-locales.gouv.fr/institutions/ctes-dematerialisation-de-la-transmission-des-actes)
à propos de la Dématérialisation de la transmission des actes.

(\*) Une liste des prestataires pour des certifiquats homologués est disponible sur [la page de l'organisme d'homologation LSTI](https://www.lsti-certification.fr/fr/psce/ "lsti").

### Affichage de la page principale ACTES

![S²LOW](/images/Prerequis/s2low_capture1.png)

### Affichage "Administration de l'entité collectivité"

![S²LOW](/images/Prerequis/s2low_capture2.png)
