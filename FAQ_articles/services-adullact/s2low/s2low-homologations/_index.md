+++
title = "Historique des homologations S2low.org"
description = "Historique des homologations S2low.org"
tags = [ "S²LOW", "S2LOW", "Homologation" ]
date = "2025-01-10"
+++
{{< toc >}}

Vous trouvez ici l'historique des homologations de la plateforme [S²LOW.org de l'Adullact](https://www.s2low.org).

* homologation S2low.org pour ACTES  : 22 janvier 2007
* homologation S2low.org pour HELIOS : mai 2009
* homologation S2low.org pour ACTES  : décembre 2011
* homologation S2low.org pour HELIOS : janvier 2016
* homologation S2low.org pour ACTES  : janvier 2016
* homologation S2low.org pour ACTES  : septembre 2019
* homologation S2low.org pour HELIOS : avril 2021
* homologation S2low.org pour ACTES  : [décembre 2024](/pdf/S2low/Homologation_ACTES_2024.pdf)
