+++
title = "Comment utiliser les APIs S²LOW en Java ?"
description = "Les applications i-Parapheur et S²LOW proposent des API (interfaces de programmation) pour les applications tierces qui souhaitent se connecter et échanger des données."
tags = [ "S²LOW", "S2LOW" ]
date = "2010-02-25"
+++
{{< toc >}}

De plus en plus de collectivités souhaitent automatiser les échanges dématérialisés, et cela passe naturellement par
la mise en place de dialogue inter-application.

Les applications i-Parapheur et S²LOW proposent des API (interfaces de programmation) pour les applications tierces
qui souhaitent se connecter et échanger des données.

Par exemple : automatiser la télé-transmission d'un ACTE administratif via S²LOW, depuis son application de gestion RH
favorite...

Les spécifications d'API de S²LOW sont publiées sur la Forge ADULLACT, et les connexions se font simplement grâce à
l'utilisation d'un certificat électronique PKCS#12.

Voici un **exemple de code JAVA** (extrait des sources de l'application i-Parapheur, logiciel libre publié sous licence
CeCILLv2), qui récupère le statut d'un ACTE précédemment télétransmis. Il montre la cinématique d'ouverture de
connexion par certificat (package Apache EasySSLProtocolSocketFactory), d'utilisation de l'API, et d'exploitation du
résultat.

```Java
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.contrib.ssl.EasySSLProtocolSocketFactory;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.protocol.ProtocolSocketFactory;
import org.apache.commons.ssl.KeyMaterial;

class ConnecteurS2low {
    private int getInfosS2lowActes(String TRANSACTION_ID)
        throws IOException, NoSuchAlgorithmException,
        KeyStoreException, KeyManagementException,
        CertificateException, UnrecoverableKeyException {

        //Map propertiesActes = getPropertiesActes();
        String passwordCertificat = "password";
        String serverAddress = "demo-s2low.exemple.org";
        String port = "443";
        int codeRetour = -1;

        EasySSLProtocolSocketFactory easy = new EasySSLProtocolSocketFactory();
        KeyMaterial km = new KeyMaterial(
            readFile("/etc/certificats/monCertificat.p12"),
            passwordCertificat.toCharArray()
        );
        easy.setKeyMaterial(km);
        Protocol easyhttps = new Protocol(
            "https",
            (ProtocolSocketFactory) easy,
            Integer.parseInt(port)
        );
        Protocol.registerProtocol("https", easyhttps);
        HttpClient client = new HttpClient();
        GetMethod get = new GetMethod(
            "https://" + serverAddress + ":" +  port +
            "/modules/actes/actes_transac_get_status.php?transaction=" + TRANSACTION_ID
        );
        try {
            int status = client.executeMethod(get);
            if (HttpStatus.SC_OK == status) {
                String reponse = get.getResponseBodyAsString();
                String[] tab = reponse.split("\n");
                if (tab.length == 1 || "KO".equals(tab[0])) {
                    String error = "Erreur retournee par la plate-forme s2low: ";
                    for (int i = 1; i < tab.length; i++) {
                        error += tab[i];
                    }
                    throw new RuntimeException(error);
                }
                // La transaction s'est bien passee, on renvoie le statut (ligne 2)
                codeRetour = Integer.parseInt(tab[1]);
            } else {
                throw new RuntimeException(
                    "Echec de la recuperation de la connexion: statut = "
                        + status);
            }
        } finally {
            get.releaseConnection();
        }
        return codeRetour;
    }
}
`̀``
