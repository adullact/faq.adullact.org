+++
title = "Premiers pas avec ASQATASUN"
description = "Le but de se document est prendre en main rapidement le service Asqatasun offert par l'ADULLACT."
tags = [ "Accessibilité", "Asqatasun", "Assistance", "Premiers pas" ]
date = "2016-09-13"
+++
{{< toc >}}

Le but de se document est prendre en main rapidement le service Asqatasun offert par l'ADULLACT.

## Connexion

Allez sur [asqatasun.adullact.org](https://asqatasun.adullact.org "Service Asqatasun de l'ADULLACT") et saisissez
votre courriel et votre mot de passe, puis cliquez sur le bouton "Se connecter".

Si vous n'avez pas encore de compte, vous pouvez consulter l'article
[FAQ - Utiliser le service Asqatasun de l'ADULLACT](../demo-asqatasun-et-creation-compte/)

![Asqatasun : Connexion Kraken](/images/Logiciels/Asqatasun/asqatasun-connexion-kraken.png)

## Accueil projet

 Une fois identifié, vous arrivez à l'Accueil projet (pour ce tutoriel, le projet porte sur le site de l'ADULLACT).

![Asqatasun ADULLACT : Exemple liste de projet](/images/Logiciels/Asqatasun/asqatasun-exemple-liste-projet.png)

## Lancer un audit

Les icônes à droite du nom du projet, permettent de lancer des audits.

Un **audit** de **page** permet d'auditer une ou plusieurs pages du site  (par exemple la page d'accueil, ou n'importe
page dont on aura copié / collé l'adresse)

## Interprétation des résultats

La mesure d'accessibilité ne peut pas être réalisée de manière entièrement automatique (certains tests nécessitent
l'intervention d'un humain, par exemple pour juger de la pertinence d'un contenu).

La note présentée par Asqatasun ne porte que sur les tests qui ont pu être automatisé sur la page en question :

* Asqatasun évalue jusqu'à ~120 tests (sur un total de 257).
* Aussi une note "**A**" signifie que tous les tests automatiques ont été passés avec succès ; les tests manuels
restant à faire.

## Limites du services Asqatasun par ADULLACT

Afin de proposer un usage égal à tous nos adhérents, certaines limites ont été mises en place:

* Les audits âgés de plus de 13 mois peuvent être supprimés pour limiter l'espace disque utilisé.

## Questions

Si vous avez des questions spécifiques au service Asqatasun créé par ADULLACT, vous pouvez écrire à
[asqatasun@adullact.org](mailto:asqatasun@adullact.org).

Pour les questions sur Asqatasun en général, n'hésitez pas à partager votre question sur le
[Forum Asqatasun](https://forum.asqatasun.org).

## Ressources pour utiliser le service Asqatasun

* [FAQ - Utiliser le service Asqatasun de l'ADULLACT pour mesurer l'accessibilité des
  sites web](../demo-asqatasun-et-creation-compte/)
* [FAQ - Autres articles sur Asqatasun et l'accessibilité](../)
