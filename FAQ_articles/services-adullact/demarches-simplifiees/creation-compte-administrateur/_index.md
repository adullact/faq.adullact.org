+++
title = "Création d'un compte collectivité sur Démarches Simplifiées ADULLACT"
description = "Premiers pas sur Démarches Simplifiées"
tags = [ "Démarches Simplifiées (DS)", "Premiers pas", "Pré-requis" ]
date = "2021-02-15"
+++
{{< toc >}}

## Présentation

Le service **[demarches.adullact.org](https://demarches.adullact.org/ "Service Démarches Simplifiées de l'ADULLACT")**
est l'instance du [logiciel libre **Démarches Simplifiées**](https://github.com/betagouv/demarches-simplifiees.fr
"Code source du logiciel libre Démarches Simplifiées sur Github")
(développé et maintenu par la [DINUM](https://www.numerique.gouv.fr/ "direction interministérielle du numérique
(DINUM)")) dédiée aux collectivités et opérée par l'ADULLACT.

**L'ADULLACT ne dispose pas d'une certification Hébergeur de Données de Santé.**

Le service ne permet pas l'hébergement et le traitement de données de santé au sens du 35 et article 4 du Règlement (UE) 2016/679,
ainsi que de l'article L1111-8 du Code de la santé publique. Entrent dans cette cathégorie trois familles de données :

* celles qui sont des données de santé par nature : antécédents médicaux, maladies, prestations de soins réalisés,
résultats d’examens, traitements, handicap, etc. (inaptitude à l’exercice d’une activité sportive, taux d'invalidité par exemple).
* celles, qui du fait de leur croisement avec d’autres données, deviennent des données de santé en ce qu’elles permettent
de tirer une conclusion sur l’état de santé ou le risque pour la santé d’une personne : croisement d’une mesure de poids
avec d’autres données (nombre de pas, mesure des apports caloriques…), croisement de la tension avec la mesure de l’effort, etc.
* celles qui deviennent des données de santé en raison de leur destination, c’est-à-dire de l’utilisation qui en est faite
au plan médical.

## Vocabulaire

* **Démarche** : démarche administrative créée par une collectivité, et à destination de ses citoyens (exemple :
inscription à la cantine, demande d'état-civil...)
* **Dossier** : ensemble des informations saisies par un citoyen lors du remplissage d'une démarche.
* **Instructeur** : agent de la collectivité chargé de traiter les dossiers d'une démarche.
* **Administrateur** : agent de la collectivité ayant les habilitations pour créer de nouvelles démarches

## Pré-requis

Le service _Démarches Simplifiées_ de l'ADULLACT s'adresse aux
[adhérents de l'association](https://adullact.org/association/adherer/formulaire-d-adhesion).

Un seul administrateur est créé **par collectivité**.
Si plusieurs personnes doivent créer des démarches ou si vous souhaitez anticiper des changements d'organisation
comme le départ d'un agent, nous vous invitons à utiliser un courriel non nominatif.

Pour demander la création du compte gestionnaire de votre collectivité adhérente, il convient de renseigner le
[formulaire de demande d'accès au service _Démarches Simplifiées_ de
l'ADULLACT](https://demarches.adullact.org/commencer/demande-d-acces-gestionnaire-de-groupe-d-administrateurs).

Ce service est mis à disposition des adhérents de l'ADULLACT. Pour des prestations complémentaires (formations,
accompagnement...) nous vous invitons à contacter les [prestataires inscrits sur le _Comptoir du_
_Libre_](https://comptoir-du-libre.org/fr/softwares/136).

## Liste de discussions

Quand une collectivité demande un accès au service _Démarches-Simplifiées_ de l'ADULLACT, nous l'inscrivons sur une
liste de diffusion dédiée à ce service. Il est possible d'y échanger entre pairs sur la création de démarches.

L'association ADULLACT utilise aussi cette liste pour informer les utilisateurs d'éventuels incidents et des évolutions
fonctionnelles, ainsi que pour organiser des _Groupes de Travail Collaboratifs_ (GTC).

Si vous utilisez déjà ce service _Démarches-Simplifiées_ de l'ADULLACT, vous pouvez écrire à
`demarches-simplifieesAROBASE listes.adullact.org` . Si ce n'est pas le cas contactez nous pour corriger cet oubli.

* * *

* [Conditions Générales pour les Administrations](../mentions-legales/cgu-pour-les-administrations/)
* [Conditions Générales d'Utilisation pour les Usagers](../mentions-legales/cgu-pour-les-usagers/)
* [Suivi d'audience et vie privée](<https://demarches.adullact.org/suivi> "Suivi d'audience et vie privée du service Démarches Simplifiées de l'ADULLACT")
* [Mentions légales](../mentions-legales/)
