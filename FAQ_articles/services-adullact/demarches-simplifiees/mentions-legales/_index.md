+++
title = "Mentions légales"
description = "Mentions légales du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++

Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour
objet de faciliter la dématérialisation des démarches administratives par toute administration.

* [Conditions Générales pour les Administrations](cgu-pour-les-administrations/)
* [Conditions Générales d'Utilisation pour les Usagers](cgu-pour-les-usagers/)
* [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
* [Déclaration d'accessibilité](accessibilite/)
* **Mentions légales**

* * *

## Mentions légales du service demarches.adullact.org

### Éditeur

[ADULLACT](https://adullact.org/), 5 rue du plan du palais - 34000 Montpellier

* Tel : 04 67 65 05 88
* Mail : `contact AROBASE adullact.org`
* RCS : Montpellier 443783170

### Directeur de la publication

Matthieu FAURE, Délégué Général de l'**ADULLACT**

### Prestataire d'infogérence

[Ouidou](https://ouidou.fr) 70-74 Boulevard Garibaldi - 75015 Paris

### Prestataire d'émission de mail

[Mailjet](https://www.mailjet.com/)  4 rue Jules Lefebvre - 75009 Paris.

### Prestataire d'hébergement

[Scaleway](https://www.scaleway.com) 8 rue de la Ville l’Evêque - 75008 Paris

