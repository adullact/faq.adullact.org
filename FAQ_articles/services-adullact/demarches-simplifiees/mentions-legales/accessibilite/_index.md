+++
title = "Accessibilité"
description = "Déclaration d'accessibilité du service \"Démarches ADULLACT\""
tags = ["Démarches Simplifiées (DS)"]
date = "2020-11-30"
geekdocCollapseSection = true
+++

Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour
objet de faciliter la dématérialisation des démarches administratives par toute administration.

* [Conditions Générales pour les Administrations](../cgu-pour-les-administrations/)
* [Conditions Générales d'Utilisation pour les Usagers](../cgu-pour-les-usagers/)
* [Suivi d'audience et vie privée](https://demarches.adullact.org/suivi)
* **Déclaration d'accessibilité**
* [Mentions légales](../)

Cette déclaration d’accessibilité s’applique à [demarches.adullact.org](https://demarches.adullact.org/).

{{< toc >}}

## État de conformité

Le site [demarches.adullact.org](https://demarches.adullact.org/)  est **partiellement conforme**
au _Référentiel Général d’amélioration de l’accessibilité_, RGAA version 4.

### Résultats des tests

En date du 13 juin 2023, le taux moyen de conformité du site est de 80%.

Les usagers peuvent suivre les mesures engagées par le service pour améliorer l’accessibilité du site à l’adresse
suivante :

Suivez le programme
d'[amélioration continue de DS en matière d'accessibilité](https://github.com/demarches-simplifiees/demarches-simplifiees.fr/issues?q=accessibilit%C3%A9+)

## Contenus non accessibles

### Non-conformités

Exemples :

* Les pages de documentation ne sont pas entièrement accessibles et sont gérées par un outil tiers.
* Il en est de même pour les pages de la FAQ.

Nous en tenons compte pour les futures évolutions des pages du site.

## Établissement de cette déclaration d’accessibilité

Cette déclaration a été établie le 27 avril 2022. Elle a été mise à jour le 1er avril 2023.

## Technologies utilisées pour la réalisation du site

* Ruby on Rails
* HTML 5
* CSS 3
* Javascript
* PDF

## Environnement de test

Les vérifications de restitution de contenus ont été réalisées sur la base d’une combinaison fournie par la base de référence du RGAA, avec les versions suivantes :

* Firefox 111.0.1 et NVDA 2022.4
* Safari et VoiceOver (version disponible sur macOS Monterey 12.6.3)
* Edge et JAWS 2020
* Google Chrome et TalkBack 9.1
* Safari et VoiceOver (version disponible sur iOS 15.7.4)

Outils pour évaluer l’accessibilité

* [Asqatasun, validateur d'accessibilité RGAA](https://asqatasun.org/)
* [Contrast-Finder, propositions de contrastes valides pour l'accessibilité](https://app.contrast-finder.org/)
* Assistant RGAA (extension Firefox)
* Web Developer (extension Firefox)
* HeadingsMap (extension Firefox)
* Application Colour Contrast Analyser
* Stylus (extension Firefox)

## Pages du site ayant fait l’objet de la vérification de conformité

Pages du site ayant fait l’objet de la vérification de conformité

* Accueil
* Contact
* Mentions légales
* Déclaration d’accessibilité
* Authentification
* Accueil connecté - Résultats de recherche usager
* FAQ
* FAQ - Usager (dépôt d'un dossier)
* Documentation - Présentation
* Documentation - Démarrage
* Documentation - Généralités
* Création dossier - Identité
* Création dossier - Brouillon
* Dossier - Résumé
* Dossier - Demande
* Dossier - Messagerie

## Retour d’information et contact

Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez contacter
le responsable du site web [demarches.adullact.org](https://demarches.adullact.org/) pour être orienté
vers une alternative accessible ou obtenir le contenu sous une autre forme.

* Envoyer un message à `support AROBASE adullact.org`
* Envoyer un courrier par la poste :
  ```text
    ADULLACT
    5 rue du plan du palais
    34000 Montpellier
  ```

## Voies de recours

Cette procédure est à utiliser dans le cas suivant.

Vous avez signalé au responsable du site internet un défaut d’accessibilité
qui vous empêche d’accéder à un contenu ou à un des services du portail
et vous n’avez pas obtenu de réponse satisfaisante.

* [Écrire un message au Défenseur des droits](https://formulaire.defenseurdesdroits.fr)
* [Contacter le délégué du Défenseur des droits dans votre région](https://www.defenseurdesdroits.fr/saisir/delegues)
* Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre)
  ```text
    Défenseur des droits
    Libre réponse 71120
    75342 Paris CEDEX 07
  ```
