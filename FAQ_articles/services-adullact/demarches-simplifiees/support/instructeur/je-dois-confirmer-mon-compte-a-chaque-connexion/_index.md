+++
title = "Je dois confirmer mon compte à chaque connexion"
description = "Je dois confirmer mon compte à chaque connexion. Documentation pour les instructeurs (traitement des dossiers) du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++

Afin de sécuriser votre compte d'instructeur ou d'administrateur, **demarches.adullact.org**
vous demande tous les mois d'authentifier votre navigateur.
Il vous faut alors cliquer sur le lien de confirmation envoyé par mail.

Ce processus peut parfois vous être demandé à chaque connexion, nous avons identifié deux raisons possibles :

- une mauvaise configuration de votre navigateur
- le navigateur authentifié n'est pas celui que vous utilisez

Finalement, le **lien** reçu par mail est **valide une semaine** et peut-être utilisé plusieurs fois.

Vous pouvez donc probablement le réutiliser pour authentifier votre navigateur sans attendre un nouveau mail.

{{< toc >}}

## Mauvaise configuration de notre navigateur

Ce problème apparaît lorsque votre navigateur est configuré de manière très sécurisée
et efface les données provenant de **demarches.adullact.org** à chaque fermeture.

Solution : Pour corriger ce problème, configurez votre navigateur pour accepter
les cookies du domaine **demarches.adullact.org** :

- pour Firefox <https://support.mozilla.org/fr/kb/sites-disent-cookies-bloques-les-debloquer>
- pour Chrome <https://support.google.com/accounts/answer/61416?co=GENIE.Platform%3DDesktop&hl=fr>

Si vous n'avez pas les droits suffisant pour modifier cette configuration,
contactez votre support informatique.

## Le navigateur authentifié n'est pas celui que vous utilisez

Il est possible que lorsque vous cliquez sur le lien du mail,
celui-ci ouvre le navigateur par défaut, la plupart du temps internet explorer.
Or le navigateur que vous utilisez pour aller sur **demarches.adullact.org** est par exemple Firefox.
Donc, le lendemain, lorsque vous ouvrez Firefox, le navigateur n'est toujours pas authentifié
et vous devez à nouveau cliquer sur le lien de connexion.

Solution : copiez le lien du mail et ouvrez-le avec le navigateur
que vous utilisez pour aller sur **demarches.adullact.org**.


