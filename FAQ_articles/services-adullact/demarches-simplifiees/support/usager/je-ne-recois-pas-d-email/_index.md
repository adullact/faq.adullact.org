+++
title = "Je ne reçois pas d'email"
description = "Je ne reçois pas d'email ? Documentation pour les usagers du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++

Si vous ne recevez pas d'email vous vous trouvez peut-être dans la situation suivante :

{{< toc >}}

## Le mail ne va pas tarder à arriver

Le mail peut mettre jusqu'à 10 minutes pour arriver.

## Le mail est arrivé dans vos courriers indésirables

Avez-vous vérifié dans vos courriers indésirables ?

## Votre compte est associé à une autre adresse email

Avez-vous vérifié que vous utilisez la bonne adresse email ?

## Vous avez fait une erreur dans la saisie de votre adresse mail

Si vous avez fait une erreur dans la saisie de votre adresse mail.
Vous pouvez [créer à nouveau un compte](https://demarches.adullact.org/users/sign_up), avec la bonne adresse.

## Vous utilisez un outil de gestion des spams (type _MailInBlack_)

Vous utilisez un outil de gestion des spams (type _MailInBlack_) qui empêche la réception des emails.
Il faut donc autoriser la réception des emails depuis `ne-pas-repondre@adullact.org`
