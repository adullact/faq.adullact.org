+++
title = "Erreur SIRET lors d'un dépôt de dossier"
description = "Erreur SIRET lors d'un dépôt de dossier. Documentation pour les usagers du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++

Cet article s'adresse exclusivement aux utilisateurs de **demarches.adullact.org**,
rencontrant un problème relatif à l'identification par numéro de SIRET lors d'un dépôt de dossiers.

> Si votre problème n'est pas relatif à l'utilisation de la plateforme **demarches.adullact.org**,
> vous pouvez consulter la page suivante:
> <https://www.service-public.fr/professionnels-entreprises/vosdroits/R17969/signaler-un-probleme>

{{< toc >}}

## Vérifiez que le numéro est bien le numéro SIRET

Le numéro SIRET contient 14 chiffres, et il est facilement confondu avec le numéro SIREN,
qui lui n'en contient que 9. Assurez-vous que le numéro que vous indiquez est bien le numéro SIRET.

### SIRET transmis pas l'URSSAF pour les jeunes structure

Pour les jeunes structures, il est possible que l'URSSAF vous ait transmis
un numéro de SIRET provisoire. Seuls les numéros de SIRET transmis par l'INSEE sont pris en compte sur notre site.

## Vérifiez la validité de votre numéro SIRET

Il est possible que le SIRET de votre entreprise ou association ait changé (suite à un déménagement par exemple,
qui entraine la fermeture de l'ancien établissement et rend le numéro de SIRET invalide), et ne fonctionne donc plus.

Pour vous assurer de la validité et du caractère public de votre SIRET, allez
sur [annuaire-entreprises.data.gouv.fr](https://annuaire-entreprises.data.gouv.fr/), rentrez votre numéro
de SIRET dans le champ de recherche, et vérifiez si votre SIRET est encore valide ou non.
S'il ne l'est plus, la page affichée vous indique alors le nouveau numéro de SIRET.

Après l'immatriculation de votre entreprise, il faut compter quelques jours avant que les informations
relatives à celle-ci ne soient disponibles et récupérables depuis
[annuaire-entreprises.data.gouv.fr](https://annuaire-entreprises.data.gouv.fr/).

## Vérifiez que les informations concernant votre entreprise sont publiques

Certaines entreprises demandent à ce que leurs informations ne soient pas accessibles dans la base publique des SIRET.
Si c'est le cas, nous ne pouvons pas récupérer les informations associées.

Pour vous assurer que les informations de votre entreprise ne sont pas privées,
allez sur l'[Annuaire des Entreprises](https://annuaire-entreprises.data.gouv.fr/)
et rentrez votre numéro de SIRET dans le champ de recherche.

## Lorsque les informations SIRET sont temporairement indisponibles

Si vous voyez un message _« Désolé, la récupération des informations SIRET est temporairement indisponible.
Veuillez réessayer dans quelques instants. »_, cela signifie que le service externe de l'INSEE qui donne
les informations d'entreprise a une panne temporaire.

La plupart du temps, le problème est résolu en quelques heures maximum.

Pour plus d'information, vous pouvez [consulter l'état du service SIRET](https://status.entreprise.api.gouv.fr/).
Si une des lignes est rouge, c'est probablement la cause du problème.
