+++
title = "Je veux supprimer mon compte"
description = "Je veux supprimer mon compte sur la plateforme demarches.adullact.org. Documentation pour les usagers du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2023-09-19"
geekdocCollapseSection = true
+++

Vous aviez créé un compte sur la plateforme de l'Adullact <https://demarches.adullact.org>.
Et maintenant, vous souhaitez supprimer ce compte.

Actuellement, le logiciel Démarches Simplifiées motorisant la plateforme n'est pas en mesure de vous rendre autonome sur cette action.

Nous acceptons les demandes de suppression par mail en écrivant à `support@adullact.org`.

Nous devons, raisonablement, nous assurer que c'est bien le ou la titulaire du compte qui nous fait la demande de suppression. C'est pourquoi, cette demande doit nous arriver depuis la même adresse mail que celle utilisée sur le compte à supprimer.
