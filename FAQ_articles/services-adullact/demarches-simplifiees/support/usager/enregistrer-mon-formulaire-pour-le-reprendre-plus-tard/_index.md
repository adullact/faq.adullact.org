+++
title = "Je veux enregistrer mon formulaire pour le reprendre plus tard"
description = "Comment enregistrer mon formulaire pour le reprendre plus tard ? Documentation pour les usagers du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++


{{< toc >}}

## Comment enregistrer mon formulaire ?

Lorsque vous remplissez un formulaire sur **demarches.adullact.org**,
les informations que vous remplissez sont **enregistrées automatiquement**.

![Copie d'écran : "Brouillon enregistré"](./ds_brouillon_autosave.png)

Si vous voulez terminer de remplir le formulaire plus tard, il suffit de **fermer** la page du **formulaire**.
Quand vous irez à nouveau sur **demarches.adullact.org**, vous pourrez reprendre votre démarche là où vous l'avez laissée.


## Comment reprendre mon formulaire plus tard ?

Si vous avez déjà commencé à remplir une démarche, vous pouvez retrouver votre dossier déjà rempli. Pour cela :

1. [Connectez-vous sur votre espace demarches.adullact.org](https://demarches.adullact.org/users/sign_in).
2. Dans la [liste de vos dossiers](https://demarches.adullact.org/dossiers),
   cliquez sur le dossier en brouillon que vous souhaitez reprendre.
3. Vous pouvez alors reprendre votre formulaire là où vous l'aviez laissé.
