+++
title = "Comment trouver ma démarche ?"
description = "Comment trouver ma démarche ? Documentation pour les usagers du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++

Pour déposer un dossier sur **demarches.adullact.org**, il est nécessaire de disposer
du lien de la démarche qui vous intéresse.

{{< toc >}}

## Comment trouver le lien de ma démarche ?

Pour déposer un dossier sur **demarches.adullact.org**, il est nécessaire de disposer
du lien de la démarche qui vous intéresse. Il ressemble à un lien de cette forme :

`https://demarches.adullact.org/commencer/NOM_DE_LA_DEMARCHE_EXEMPLE`

Ce lien vous est communiqué par l’administration compétente pour votre démarche :
généralement sur son site internet, ou par email. Nous vous invitons à contacter l’administration
en charge de votre démarche pour qu’elle vous indique le lien pour déposer votre dossier.

## Je ne trouve pas la démarche que je veux faire

Il est possible que l’administration en charge de votre démarche n’ait pas choisi d’utiliser **demarches.adullact.org**
pour dématérialiser celle-ci. Dans ce cas-là, nous vous invitons à contacter l’administration en charge de votre démarche
pour qu’elle vous indique la procédure à suivre.

Vous pouvez aussi vous rendre sur [service-public.fr](https://www.service-public.fr)
qui référence la plupart des démarches administratives.

## Je veux compléter un dossier déjà créé

Pour compléter un dossier sur _demarches.adullact.org_ :

- [Connectez-vous sur votre espace demarches.adullact.org](https://demarches.adullact.org/users/sign_in).
- Renseignez alors l’adresse email et le mot de passe utilisés lors de la création de votre dossier.

## Je veux déposer un nouveau dossier

Pour commencer un nouveau dossier sur une démarche que vous avez déjà réalisée :

- [Connectez-vous sur votre espace demarches.adullact.org](https://demarches.adullact.org/users/sign_in).
- Cliquez ensuite sur le bouton "Actions" du dossier correspondant à la démarche que vous souhaitez faire.
- Sélectionnez enfin le bouton "Commencer un autre dossier".
