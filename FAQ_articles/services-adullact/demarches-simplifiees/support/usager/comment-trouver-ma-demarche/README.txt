/!\ ne pas déplacer de ce dossier
----------------------------------------------------------

L'URL suivante est utilisée directement par le service DS de l'Adullact
/services-adullact/demarches-simplifiees/support/usager/comment-trouver-ma-demarche/

Si vous souhaitez déplacer ce dossier, il faut en complément :
1. ajouter une redirection dans le fichier .gitlab/ci/build_additional_files/.htaccess
2. modifier la configuration de l'instance DS (chaine i18n "inks.common.faq.comment_trouver_ma_demarche_url"

---------------------------------------------------------
Source d'inspiration :
https://faq.demarches-simplifiees.fr/article/77-enregistrer-mon-formulaire-pour-le-reprendre-plus-tard
