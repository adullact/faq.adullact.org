+++
title = "Documentation - Gestionnaire (création de comptes administrateurs)"
description = "Documentation pour les gestionnaires du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2024-06-03"
geekdocCollapseSection = true
+++
{{< toc >}}

## 1. Tutoriel Gestionnaire de groupe

Ce tutoriel a pour objet de présenter le profil de gestionnaire
de groupe ainsi que sa principale fonctionnalité qui est de
pouvoir créer des administrateurs dans l’application
Démarches Simplifiées.

### 1.1 Qu'est-ce qu'un gestionnaire ?

Le gestionnaire a pour rôle principal de pouvoir créer des
administrateurs et de gérer les comptes créés par lui-même
dans un périmètre défini (un groupe).

**Le gestionnaire de groupe peut :**

- créer des administrateurs
- révoquer ou retirer un administrateur d'un groupe
- créer d'autres gestionnaires
- créer des sous-groupes (que nous appelons des groupes enfants dans
l'application)

**Le gestionnaire ne peut pas :**

- gérer des démarches
- gérer des dossiers
- gérer des instructeurs et/ou des experts
- gérer des administrateurs d'un groupe dont il n'a pas la gestion.

> Pour information : Un utilisateur gestionnaire créé sera automatiquement
doté du statut d'administrateur.

### 1.2 Découvrir la notion de groupe gestionnaire

Le groupe gestionnaire est une nouvelle entité créée afin de définir un
périmètre d'intervention (exemple : une collectivité au sein d'une
intercommunalité, un service dans une collectivité...).

Dans ce groupe figure les profils suivants :

- le/les gestionnaire·s
- le/les administrateur·s

![tableau_de_bord](/images/Logiciels/demarches/doc-gestionnaire/ecran1.png)

#### Puis-je faire partie de plusieurs groupes ?

- **Oui**, si je suis **gestionnaire**, je peux appartenir à plusieurs
groupes. Par ailleurs je vais pouvoir être à mon tour créateur de
groupes (groupes enfants)

- **Non**, si je suis un **administrateur** créé par un gestionnaire,
je suis rattaché à un seul et unique groupe.

Les démarches et les dossiers ne sont pas impactés par cette notion de
groupe. Ils continuent à être utilisés de la même manière.

> Attention : La notion de groupe gestionnaire est à ne pas confondre avec
celle des **groupes d'instructeurs** qui est proposé dans le contexte de
routage d'instruction des dossiers.

### 1.3 Demande de création d'un compte gestionnaire

Rendez-vous sur la page [formulaire de demande d'accès au service Demarches de l'Adullact](https://demarches.adullact.org/commencer/demande-d-acces-gestionnaire-de-groupe-d-administrateurs)

Une fois votre demande traitée et acceptée, vous recevrez un e-mail vous
invitant à créer votre mot de passe pour pouvoir accéder à votre compte.

## 2. Découvrir son tableau de bord de gestionnaire

### 2.1 La page d'accueil

Une fois connecté en gestionnaire, vous accédez au tableau de bord.

Vous pourrez :

- visualiser une liste d'un ou plusieurs « groupes » dont vous êtes
nommé gestionnaire
- cliquer sur un groupe pour accéder à la gestion de celui-ci.
- visualiser des informations statistiques relatives à vos groupes
(nombre de gestionnaires et d'administrateurs que contient le groupe.)

![tableau_de_bord](/images/Logiciels/demarches/doc-gestionnaire/ecran1.png)

### 2.2 Accéder aux informations d'un groupe

En cliquant sur le nom d'un groupe, vous accédez aux fonctions dudit
groupe :

- « gérer les gestionnaires »
- « gérer les administrateurs »
- voir la messagerie du groupe
- créer un groupe enfant à partir de ce groupe

![groupe](/images/Logiciels/demarches/doc-gestionnaire/ecran2.png)

### 2.3 Gérer les gestionnaires d'un groupe

- Le gestionnaire peut créer d'autres gestionnaires dans un groupe.
(création de compte)
- Il peut également retirer un gestionnaire d'un groupe

![gestion_groupe](/images/Logiciels/demarches/doc-gestionnaire/ecran3.png)

Pour ajouter un gestionnaire :

1. saisissez le courriel d'un gestionnaire
2. cliquez sur le bouton « Ajouter comme gestionnaire »
3. votre Gestionnaire est ajouté au groupe.

![gestionnaire](/images/Logiciels/demarches/doc-gestionnaire/ecran4.png)

Puis, votre gestionnaire recevra deux messages électroniques :

- un message de confirmation d'ajout au groupe en tant que gestionnaire
- un message d'activation du compte gestionnaire

![message_activation](/images/Logiciels/demarches/doc-gestionnaire/ecran14.png)
et
![message_-_ajout_groupe](/images/Logiciels/demarches/doc-gestionnaire/ecran15.png)

### 2.4 Gérer les administrateurs

#### A. Créer un nouvel administrateur

La fonction principale du gestionnaire est de pouvoir créer de nouveaux
comptes administrateurs en toute autonomie sans devoir faire appel au
Super-Admin.

Pour ce faire, il convient d'aller dans le menu « gérer les
administrateurs », puis d'ajouter le courriel du futur administrateur et
de valider le processus de création.

![ajout_gestionnaire](/images/Logiciels/demarches/doc-gestionnaire/ecran5.png)

Des messages électroniques seront automatiquement envoyés à l'adresse
mail définie :

- un message pour activer le compte administrateur et y associer un mot
de passe
- un message informatif pour que l'administrateur sache à quel groupe
il appartient

#### B. Gérer les administrateurs de son groupe

Le gestionnaire peut effectuer les actions suivantes :

- consulter la liste des administrateurs
- ajouter un nouvel administrateur par le biais de son courriel
- retirer un administrateur du groupe
- révoquer un administrateur (retrait du groupe et révocation du rôle
d'administrateur)

![ajout_admin](/images/Logiciels/demarches/doc-gestionnaire/ecran6.png)

#### C. Le retrait d'un groupe

La fonction « Retirer du groupe » permet à un gestionnaire de pouvoir
retirer un administrateur d'un groupe sans pour autant supprimer son
rôle d'administrateur.

L’administrateur sera alors sans groupe gestionnaire et pourra continuer
à administrer ses démarches.

> Pour rappel, l'appartenance à un groupe ne permet pas de disposer de
droits spécifiques sur les démarches des administrateurs gérés.

![retrait](/images/Logiciels/demarches/doc-gestionnaire/ecran7.png)

#### D. La révocation de droits

La fonction **Révoquer l'accès administrateur** permet à un gestionnaire
de supprimer un administrateur d’un groupe mais également de supprimer
son rôle d’administrateur sur la plateforme. À la révocation, le compte
en question pourra en revanche se connecter en simple utilisateur ou
expert/instructeur si celui-ci a été nommé au préalable sur des démarches
ou des dossiers.

Avant la suppression du rôle, un contrôle de cohérence est effectué afin
de ne pas supprimer un administrateur qui serait seul à administrer une
démarche. Dans ce cas précis, l'administrateur ne pourra pas être
supprimé.

![revoquer](/images/Logiciels/demarches/doc-gestionnaire/ecran8.png)

### 2.5. Le principe de la délégation de droits de création

#### A. Le groupe enfant

Le concept d'affiliation à des groupes a fait naître un besoin pour le
gestionnaire de pouvoir déléguer à son tour la création de comptes
administrateurs.

Pour ce faire, le gestionnaire à un menu qui lui permet de créer à son
tour des sous groupes de son groupe. Dans l'application nous parlons de
« Groupes Enfants ».

![groupe_enfant](/images/Logiciels/demarches/doc-gestionnaire/ecran9.png)

Pour créer un groupe enfant, le gestionnaire saisit le nom du groupe
puis clique sur le bouton « Ajouter un nouveau groupe ».

Le groupe créé apparaît ainsi dans la liste des groupes enfants.

Le gestionnaire a alors accès aux même actions pour son groupe enfant
que celles de son groupe. (ajouter des gestionnaires / ajouter des
administrateurs / visualiser et créer des groupes enfants) sans limite
de profondeur.

#### B. L'arborescence

Visualiser l'arborescence permet de savoir situer son groupe dans
l'architecture des groupes existants.

Elle permet :

- d'identifier son groupe parent
- de visualiser ses groupes enfants et leurs enfants

![arborescence](/images/Logiciels/demarches/doc-gestionnaire/ecran10.png)

Le gestionnaire peut ajouter :

- plusieurs groupes enfants sur un seul niveau d'arborescence
- plusieurs groupes enfants de différents niveaux et ce, sans limite de
profondeur. (enfants, petits-enfants, arrières petits-enfants, etc.)

## 3. La messagerie

Un système de messagerie est disponible dans la gestion de groupe. Cette
messagerie est visible par les gestionnaires ainsi que par les
administrateurs affiliés à un groupe.

La messagerie permet à un gestionnaire de :

- contacter le gestionnaire du groupe parent
- être contacté par un administrateur de son groupe
- être contacté par un gestionnaire d'un groupe enfant

La messagerie permet à un administrateur de :

- contacter son gestionnaire de groupe.

### 3.1 Écriture du message

Pour pouvoir envoyer un message il suffit d'aller dans le module
« Messagerie » du groupe.

Saisissez votre message et cliquez sur le
bouton « Envoyer le message »

![ecriture_message](/images/Logiciels/demarches/doc-gestionnaire/ecran11.png)

Le message est automatiquement envoyé aux gestionnaires du groupe ciblé.

### 3.2 réception du message

Les gestionnaires peuvent visualiser la messagerie d'une manière plus
complète :

Messages reçus :

- messagerie du groupe (par instructeur)
- messagerie des groupes enfants (par gestionnaire et par groupe)
- contact vers le gestionnaire du groupe parent

![messagerie](/images/Logiciels/demarches/doc-gestionnaire/ecran12.png)
