+++
title = "Support"
description = "Déclaration d'accessibilité du service \"Démarches Simplifiées\" ADULLACT"
tags = [ "Démarches Simplifiées (DS)" ]
date = "2020-11-30"
geekdocCollapseSection = true
+++

{{< toc >}}

## Votre question

### J’ai un problème lors du remplissage de mon dossier

- Avez-vous bien vérifié que tous les champs obligatoires ( * ) sont remplis ?
- Si vous avez des questions sur les informations à saisir, contactez les services en charge de la démarche.

### Je ne trouve pas la démarche que je veux faire

Nous vous invitons à contacter l’administration en charge de votre démarche
pour qu’elle vous indique le lien à suivre. Celui-ci devrait ressembler à cela :
`https://demarches.adullact.org/commencer/NOM_DE_LA_DEMARCHE_EXEMPLE`.

### J’ai une question sur l’instruction de mon dossier

Si vous avez des questions sur l’instruction de votre dossier (par exemple sur les délais),
nous vous invitons à contacter directement les services qui instruisent votre dossier par votre messagerie.

### Je ne reçois pas d'email

Si vous ne recevez pas d'email vous vous trouvez peut-être dans la situation suivante :

- Le mail est arrivé dans vos courriers indésirables. Avez-vous vérifié dedans ?
- Votre compte est associé à une autre adresse email. Avez-vous bien vérifié la bonne ?
- Vous avez fait une erreur dans la saisie de votre adresse mail.
  Vous pouvez [créer à nouveau un compte](https://demarches.adullact.org/users/sign_up), avec la bonne adresse.
- Vous utilisez un outil de gestion des spams (type _MailInBlack_) qui empêche la réception des emails.
  Il faut donc autoriser la réception des emails depuis `demarches.adullact.org`

## Questions spécifiques : instructeurs

### J'ai un instructeur qui n'est plus présent

Nous sommes dans la situation où une personne titulaire d'un compte instructeur n'est plus présente afin d'instruire
une démarche de votre organisme pour laquelle elle dispose de cette habilitation. Par exemple, la personne titulaire
du compte instructeur a quitté votre organisme, ou la personne est affectée à une nouvelle mission.

Il convient d'utiliser un compte administrateur de la démarche. Ce dernier dispose des habilitations à agir sur cette
situation. Il peut par exemple ajouter un nouvel instructeur ou supprimer l'instructeur vacant.

## Universités, établissements d'enseignement supérieur et de recherche

Pour les **universités**, les établissements d'enseignement supérieur et de recherche, le [support est assuré
par l'**AMUE**](https://www.amue.fr/systeme-dinformation/metier/demarches-simplifiees-du-sup/a-propos/services-complementaires/).


## Contact technique


- Le support technique ne s'occupe que des questions techniques liées à l'utilisation du site.
- Il ne pourra pas répondre aux questions concernant votre dossier ou le traitement de votre demande par l'administration.
  Pour une question administrative, contactez plutôt l’administration en charge de votre dossier.


### J’ai un problème technique

Si vous souhaitez poser une question pour un problème technique sur le site [demarches.adullact.org](https://demarches.adullact.org/),
vous pouvez evnoyer un message à l'adresse mail suivante :
> `support AROBASE adullact.org`
