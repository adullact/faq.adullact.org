+++
title = "Documentation - API GraphQL"
description = "Documentation pour utiliser l'API GraphQL de \"Démarches Simplifiées\" ADULLACT"
tags = ["Démarches Simplifiées (DS)"]
date = "2024-10-22"
geekdocCollapseSection = true
+++

{{< toc >}}

## Introduction

L'API (Application Programming Interface) permet d'interconnecter Demarches Simplifiées
à d'autres systèmes d'information ou d'autres logiciels.

Aujourd'hui l’API est sortante et entrante. Vous pouvez donc aussi bien lire qu'écrire
des données dans Démarches Simplifiées.

Elle permet par exemple de récupérer les données au format json pour:

- Construire des tableaux bord afin de piloter le déploiement d'une démarche
- Utiliser Démarches Simplifiées pour ses formulaires et verser les dossiers saisis
  par les usagers dans votre Système d'Information afin de les instruire vous-même.
- Construire une interface sur mesure pour instruire les dossiers dans votre application

Que vous pourrez ensuite intégrer dans des outils métiers.

## Paramétrages

### Pré-requis

- avoir un compte administrateur sur demarches.adullact.org
- avoir une ou plusieurs démarches publiées sur ce compte administrateur
- configurer des jetons GraphQL sur la plateforme DS

La fonctionnalité est activée par défaut dans le code de l'application.

### Données techniques de l’API

L'API est accessible à l'adresse /api/v2/graphql (cette URL ne peut pas être consultée dans
un navigateur web : code HTTP 404 Page Not Found).

Une fois authentifié en tant qu'administrateur disposant d'un jeton, vous pouvez également
accéder à l'éditeur de requêtes en ligne  `/graphql` (attention, ne confondez pas cette adresse avec celle du
*endpoint*).

Endpoint: <https://demarches.adullact.org/api/v2/graphql>

## Configurer des jetons GraphQL

**Attention** : il est nécessaire d’avoir une accréditation au profil « Administrateur »
dans l’application.

Pour configurer le/les jeton(s), vous devez vous rendre sur votre profil utilisateur de
votre compte associé à Démarches Simplifiées.

1. Sélectionnez « voir mon profil dans le menu de sélection du rôle »
   ![profil](/images/Logiciels/demarches/doc-APIGraphql/profil.png)
2. Une fois sur la page /profil, allez sur l’encart « Jetons d’identification de l’API (token)»
   ![profil](/images/Logiciels/demarches/doc-APIGraphql/créer_jeton.png)
3. Cliquez sur « créer un nouveau jeton »
4. Définissez le nom et les privilèges de ce jeton (pour une ou plusieurs démarches spécifiques,
   en lecture ou écriture…) et leur niveau de sécurité
5. Validez

Le jeton est alors créé. Avec celui-ci, vous pourrez accéder à l’api et exporter les informations
souhaitées dans le périmètre que vous avez établi pour ce jeton.

Ce jeton doit être fourni dans l’en-tête HTTP « Authorization » de la requête.

```apacheconf
Authorization: Bearer token=valeur_du_jeton
```

## Accéder à l’Api

Pour le service demarches.adullact.org, voici l’URL d’accès à l’Api:

<https://demarches.adullact.org/graphql>

## Exemples de récupération via l’API

L'utilisation de l'API permet de récupérer un certain type de données comme par exemple :

- de lister les dossiers d’une démarche
- des informations complètes des dossiers
- les dossiers d’un groupe d’instructeur
- les versions du formulaire par démarche

ou en écriture:

- de changer l’état d’un dossier (accepté, refusé…)

## Exemples de requête et affichage des résultats

Je souhaite récupérer la liste des dossiers d'une démarche depuis l'API

### Requête GraphQL

```graphql
query exempleListeDossiers($demarcheNumber: Int!) {
  demarche(number: $demarcheNumber) {
    id
    number
    state
    title
    dossiers {
      nodes {
        number
        state
        usager {
          email
        }
        traitements {
          dateTraitement
          state
          emailAgentTraitant
        }
      }
    }
  }
}
```

### Variables associées à la requête GraphQL

```json
{
  "demarcheNumber": 339
}
```

### Résultat renvoyé par GraphQL

```json
{
  "data": {
    "demarche": {
      "id": "UHJvY2VkdXJlLTMzOQ==",
      "number": 339,
      "state": "publiee",
      "title": "ADULLACT - Démo n°2  : démarche DS pour les organisations (entreprise, association...)",
      "dossiers": {
        "nodes": [
          {
            "number": 12126,
            "state": "accepte",
            "usager": {
              "email": "john.doe@example.org"
            },
            "traitements": [
              {
                "dateTraitement": "2022-05-31T12:33:48+02:00",
                "state": "en_construction",
                "emailAgentTraitant": null
              },
              {
                "dateTraitement": "2022-06-01T11:20:02+02:00",
                "state": "en_instruction",
                "emailAgentTraitant": "instructeur-1@example.org"
              },
              {
                "dateTraitement": "2022-06-01T11:20:46+02:00",
                "state": "accepte",
                "emailAgentTraitant": "instructeur-1@example.org"
              }
            ]
          },
          (...)
        ]
      }
    }
  }
}
```

### Jouer la Requête avec CURL

```shell
# Jeton DS pour l'API GraphQL
set +o history # désactiver l'historique des lignes de commande
API_JETON="<YOUR_VALID_TOKEN>"
set -o history # activer l'historique des lignes de commande

API_GRAPHQL_URL="https://demarches.adullact.org/api/v2/graphql"
curl -X POST "${API_GRAPHQL_URL}" \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer token=${API_JETON}" \
  --data-raw '{"variables":{"demarcheNumber":339}, "query":"query exempleListeDossiers($demarcheNumber: Int!){ demarche(number: $demarcheNumber) { number title dossiers { nodes { number state usager { email } traitements { dateTraitement state emailAgentTraitant }}}}} "}'
```

### Vue de la requête depuis le bac à sable GraphQL de DS

<https://demarches.adullact.org/graphql>

![requete1](/images/Logiciels/demarches/doc-APIGraphql/requete1.png)

## Documentation générale à l'usage des développeurs

Vous retrouverez une documentation technique plus détaillée sur le site de la DINUM :

<https://doc.demarches-simplifiees.fr/api-graphql>
