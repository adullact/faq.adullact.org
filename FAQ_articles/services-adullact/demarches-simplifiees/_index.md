+++
title = "Démarches Simplifiées"
geekdocCollapseSection = true
+++

Le site [demarches.adullact.org](https://demarches.adullact.org/), est un service édité par l’ADULLACT qui a pour
objet de faciliter la dématérialisation des démarches administratives par toute administration.

{{< toc-tree >}}
