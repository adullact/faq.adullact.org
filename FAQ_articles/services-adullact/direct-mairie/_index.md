+++
title = "Direct-Mairie"
geekdocCollapseSection = true
+++

L'application DirectMairie est un service libre de remontée d'information citoyenne, permettant d'effectuer
un signalement géolocalisé (dépôt sauvage d'encombrants, éclairage public défaillant, nid de poule, etc.)
depuis un téléphone mobile ou un ordinateur de bureau auprès de sa collectivité; de manière anonyme ou non.

{{< toc-tree >}}
