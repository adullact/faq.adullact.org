+++
title = "Débuter sur DirectMairie"
description = "Debuter sur \"DirectMairie\" ADULLACT"
tags = [ "Assistance", "DirectMairie", "Premiers pas", "Debuter" ]
date = "2020-04-22"
+++
{{< toc >}}

## Déroulé côté utilisateur

### Se rendre sur le service

La première fois, l’utilisateur commence par lancer un navigateur sur son périphérique (smartphone, tablette, …) avec le chemin (url): [https://directmairie.adullact.org](https://directmairie.adullact.org)

Selon le navigateur utilisé, il peut ajouter un raccourci sur son écran ou télécharger depuis les stores Apple et Google Play.

* [AppleStore](https://apps.apple.com/fr/app/directmairie/id1613344892)
* [GooglePlayStore](https://play.google.com/store/apps/details?id=org.adullact.directmairie&hl=fr&gl=US)

### Remonter une information

Il suffit ensuite de cliquer sur `remonter une information`. Plusieurs opérations sont proposées:

* Localiser le problème en cliquant sur `ma position` (sinon se situer manuellement sur la carte).
* Sélectionner la catégorie du problème, parmi la liste proposée (cette liste est configurable selon la collectivité).
* Décrire le problème, en quelques mots.
* Et enfin, éventuellement ajouter une photo prise avec l’appareil.

Puis envoyer ces informations à la collectivité correspondant à la géolocalisation (ci-dessus).

`Si la commune géolocalisée n’a pas encore configuré DirectMairie pour son territoire, un message d’erreur apparait alors, dès l’étape n°2. L’utilisateur est ainsi prévenu qu’il ne peut pas utiliser DirectMairie sur ce territoire.
Sinon, la fiche ainsi créée est alors envoyée au service concerné de la mairie sélectionnée.`

### Création de compte (Facultatif)

L’utilisateur peut ensuite se créer un compte si celui-ci le souhaite afin de suivre le traitrement de ses remontées. (non obligatoire) et la gestion des ses abonnements (alertes collectivités)

#### Abonnement aux alertes

L'utilisateur inscrit à la possibilité de s'abonner aux alertes en provenance de sa/ses collectivités préférées. Pour cela il peut :

* soit s'y inscrire directement après une remontée effectuée
* soit en allant sur le menu `Mon compte` et accéder à la page `Mes abonnements`

Pour se désabonner, l'utilisateur devra accéder au menu `Mon compte` et accéder à la page `Mes abonnements`

#### Consultation de l'historique de ses signalements

Cette information est uniquement visible si l'utilisateur a créé un compte et à fait des signalements avec celui-ci.

Les informations sont visibles sur la page d'accueil de l'application en dessous du bouton `Remonter une information.`

Il pourra également vérifier les évolutions de ses remontées ainsi que les messages laissés par les agent en charge du traitement.

## Déroulé côté administrateur de collectivité

### Traitement des remontées

L'administrateur de collectivité pourra via le menu `Remontées d'information` :

* traiter les remontées effectuées sur son territoire (résoudre ou rejeter) et y associer un motif de traitement
* exporter au format cnv les remontées existantes

### Envoi d'alertes aux utilisateurs abonnés

L’administrateur de collectivité pourra, s’il le souhaite, envoyer des messages d’alertes par mail aux usagers abonnés au flux de sa collectivité par le biais du menu `Alertes` présent dans l’application.
