+++
title = "Support"
description = "Support \"DirectMairie\" ADULLACT"
tags = [ "Assistance", "DirectMairie" ]
date = "2023-01-25"
geekdocCollapseSection = true
+++

{{< toc >}}

## J'ai perdu mon mot de passe et je ne peux plus me connecter

> Depuis l'application, je clique sur s'`identifier` puis `mot de passe oublié`. Une fois l'adresse mail renseignée vous allez recevoir un mail dans votre messagerie qui vous permettra de modifier votre mot de passe.

## Je ne reçois pas d'email

> Si vous ne recevez pas d’email vous vous trouvez peut-être dans la situation suivante :

- Le mail est arrivé dans vos courriers indésirables. Avez-vous vérifié dedans ?
- Votre compte est associé à une autre adresse email. Avez-vous bien vérifié la bonne ?
- Vous avez fait une erreur dans la saisie de votre adresse mail. Vous pouvez créer à nouveau un compte, avec la bonne adresse.
- Vous utilisez un outil de gestion des spams (type MailInBlack) qui empêche la réception des emails. Il faut donc autoriser la réception des emails depuis directmairie.adullact.org

## J'ai un problème technique

> Le support technique ne s’occupe que des questions techniques liées à l’utilisation du site. (Exemple "un message d'erreur est affiché")

Si vous souhaitez poser une question pour un problème technique sur le site directmairie.adullact.org, vous pouvez envoyer un message à l’adresse mail suivante :

support AROBASE adullact.org

## Ma remontée n'est pas traîtée, je souhaite avoir plus d'information quant à son traitement

> Je dois contacter ma collectivité.

## Je souhaite modifier ou supprimer mes informations personnelles

> je peux accéder à `Mon Compte` modifier ou supprimer mes informations afin d'effectuer ces manipulations.
