+++
title = "Catégories de thèmes et sous thèmes"
description = "Catégories de thèmes et sous thèmes \"DirectMairie\" ADULLACT"
tags = [ "DirectMairie" ]
date = "2023-01-25"
geekdocCollapseSection = true
+++

{{< toc >}}

## Liste des thèmes et sous-thèmes proposés par l'application

Voici la liste des catégories créées par défaut. Elle s’appuient sur les travaux menés par la Ville de Paris (pour son application Dans ma rue, application orientée vers les très grandes collectivités).

* Graffiti, tags et affiches
  * Affiches ou autocollants
  * Graffiti
  * Inscription haineuse
* Mobilier urbain
  * Abribus détérioré
  * Banc détérioré
* Objet abandonné
  * Autres objets encombrants abandonnés
  * Bac roulant à ordures ménagères
  * Chariot de supermarché
  * Gravats ou déchets de chantier
  * Sacs de déchets ménagers
* Propreté
  * Bac à ordure ménagères sale ou débordant
  * Colonne à verre sale ou débordante
  * Conteneur pour la collecte des textiles sale ou débordant
  * Déchet et/ou salissures
  * Déjections canines
  * Épanchement d'urine
* Voirie et espace public
  * Affaissement, trou, bosse, pavé arraché
  * Feu tricolor éteint ou cassé
  * Itinéraire cyclable interrompu
  * Obstacle sur la piste cyclable
* Éclairage public
  * Éclairage public allumé le jour
  * Éclairage public éteint la nuit
* Épave auto, moto, vélo
  * Épave automobile
  * Épave deux-roues motorisé
  * Épave vélo
* Autre

Ces thèmes sont paramétrables par collectivité.

## Soumettre un nouveau thème / sous-thème

Si vous constatez qu'un thème est manquant, merci d’envoyer un courriel à support AROBASE adullact.org (remplacer AROBASE par @)

Nous analyserons votre demande afin d’enrichir la liste pré-existante.
