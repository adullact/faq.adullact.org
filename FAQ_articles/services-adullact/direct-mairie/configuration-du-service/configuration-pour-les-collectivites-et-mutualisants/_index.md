+++
title = "Configuration pour les collectivités et les mutualisants"
description = "Configuration \"DirectMairie\" ADULLACT"
tags = [ "DirectMairie" ]
date = "2023-01-25"
geekdocCollapseSection = true
+++

{{< toc >}}

## Configuration pour les collectivités et les mutualisants

### Configuration pour les collectivités

Une page est dédiée à la collectivité. Il est possible de :

- Personnaliser (logo, url)
- Modifier la liste des catégories et sous catégories affichées,
- Modifier les courriels attachés à ces catégories (par défaut c’est le courriel de l’administrateur initial qui est utilisé)
  
### Configuration pour les mutualisants

L’administrateur de mutualisant peut configurer l’application pour sa/ses collectivité(s). Il est possible de :

- Personnaliser (logo, url)
- Modifier la liste des catégories et sous catégories affichées,
- Modifier les courriels attachés à ces catégories (par défaut c’est le courriel de l’administrateur initial qui est utilisé)
- Mais également de :
  - créer d’autres administrateurs.
  - Créer des collectivités qui lui seront rattachées
