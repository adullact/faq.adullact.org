+++
title = "Ouverture d'un compte pour les adhérents Adullact"
description = "Ouverture d'un compte pour les adhérents Adullact \"DirectMairie\" ADULLACT"
tags = [ "DirectMairie" ]
date = "2023-01-25"
geekdocCollapseSection = true
+++

## Ouverture d'un compte pour les adhérents Adullact

1. En tant que gestionnaire de collectivité, vous devez vous [créer un compte citoyen](https://directmairie.adullact.org/registration).
2. Puis renseigner la demande d’accès à DirectMairie pour votre collectivité [depuis le formulaire d'inscription au service DirectMairie](https://demarches.adullact.org/commencer/inscription-au-service-directmairie-de-l-adullact)
3. Une fois la démarche validée vous aurez accès à des menus supplémentaires dans l’application. Soit en tant que mutualisant, soit en tant que gestionnaire de collectivité.
