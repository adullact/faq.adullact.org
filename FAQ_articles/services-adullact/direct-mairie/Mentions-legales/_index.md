+++
title = "Mentions Légales"
description = "Mentions-legales \"DirectMairie\" ADULLACT"
tags = [ "DirectMairie" ]
date = "2023-01-25"
geekdocCollapseSection = true
+++

{{< toc >}}

### Politique de confidentialité

Toutes les informations sont disponibles sur l'application à cette adresse :

[https://directmairie.adullact.org/confidentiality](https://directmairie.adullact.org/confidentiality)

### Accessibilité

Non Conforme
