+++
title = "Publication des données essentielles de la commande publique"
description = "Publication des données essentielles de la commande publique"
tags = [ "Web-marché", "Marchés publics", "DECP"]
date = "2024-02-01"
+++
{{< toc >}}

Le profil acheteur de l'Adullact nommé [Webmarché](https://webmarche.adullact.org) permet de se conformer
à la législation concernant la publication de la donnée essentielle des marchés publics.

## Réglementation

Le décret n° 2022-767 du 2 mai 2022 portant diverses modifications du code de la commande
publique renforce la démarche d’ouverture des données (open data), notamment en
imposant la publication des données essentielles de la commande publique sur une seule et
unique plateforme : le portail national des données ouvertes.

## Où trouver les données publiées ?

Les données essentielles de la commande publique du profil d'acheteur de l'Adullact sont disponibles sur le portail national des données ouvertes, alias [data.gouv.fr](https://www.data.gouv.fr), au travers d'un [jeu de données mis à jour de façon hebdomadaire](https://www.data.gouv.fr/fr/datasets/donnees-essentielles-du-profil-dacheteur-webmarche-adullact/).

