+++
title = "Premiers pas en production avec web-marché"
description = "Premiers pas en production avec web-marché"
tags = [ "Web-marché", "Marchés publics", "Premiers pas" ]
date = "2011-08-18"
+++
{{< toc >}}

Cet article indique les pré-requis pour un démarrage sur la plateforme de dématérialisation des marchés publics de
l'ADULLACT, nommée Web-marché.

La collectivité adhérente souhaitant démarrer avec Web-marché devra fournir certains renseignements afin de permettre
à nos équipes de créer le compte. La plateforme de l'ADULLACT est accessible aux collectivités adhérentes :

* pour les collectivités : [https://webmarche.adullact.org/agent](https://webmarche.adullact.org/agent "Accès collectivités")
* pour les entreprises : [https://webmarche.adullact.org](https://webmarche.adullact.org "Accès entreprise")

Les collectivités adhérents à l'association doivent demander la création d'un compte administrateur de la collectivité pour son profil acheteur. Pour cela, il
convient de renseigner le
[formulaire de demande d'accès au Webmarché de l'ADULLACT](https://demarches.adullact.org/commencer/inscription-au-service-webmarche-de-l-adullact "Formulaire de demande d'accès pour les collectivités").

Les entreprises peuvent librement créer leur compte sur la [plateforme Web-marché](https://webmarche.adullact.org "Accès entreprise")
