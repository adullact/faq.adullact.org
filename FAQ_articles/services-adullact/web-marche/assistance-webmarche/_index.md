+++
title = "Comment recevoir de l'aide sur Web-marché ?"
description = "Page d'aide pour accéder à Web Marché"
tags = [ "Web-marché", "Marchés publics", "Assistance" ]
date = "2018-11-02"
+++
{{< toc >}}

En cas de difficultés, plusieurs possibilités s'offrent à vous :

1. les manuels d'utilisation du logiciel
2. la liste de discussions entres nos utilisateurs
3. l'assistance technique via UTAH

## Les manuels d'utilisation du logiciel

Vous disposez des manuels d'utilisation du logiciel directement sur la plateforme
[https://webmarche.adullact.org](https://webmarche.adullact.org). Il existe un manuel pour les agents et les
administrateurs de collectivité.

Pour cela, vous devez vous [connecter en tant qu'agent de la collectivité](https://webmarche.adullact.org/?page=Agent.AgentHome "Accès agent") et vous rendre dans la rubrique `Aide > Guides d'utilisation`.

## La liste de discussions

Quand une collectivité demande un accès au service Web-marché, nous inscrivons l'administrateur sur une liste de
diffusion dédiée aux marchés publics. Il est possible d'y échanger entre pairs.

Si vous administrez une collectivité sur Web-marché, vous pouvez écrire à `marchespublics@listes.adullact.org` .
Si ce n'est pas le cas contactez nous pour corriger cet oublie.

## L'assistance technique via UTAH

L'ADULLACT offre à ses membres la mise à disposition d'une assistance en cas de problème technique sur sa plateforme
de marchés publics. Voilà la procédure à suivre pour accéder à cette assistance.

Une languette permet d'accéder à l'assistance en ligne est réduite à droite de votre écran.

Vous pouvez voir un exemple ci-dessous sur la page d’accueil.

![Page d'accueil](/images/Logiciels/web-marche/webmarche-accueil.png)

Déplacez votre souris sur la languette, elle s'agrandit. Vous pouvez alors cliquer dessus.

![Zoom](/images/Logiciels/web-marche/webmarche-zoom.png)

Sélectionnez alors la catégorie qui concerne votre demande dans "Ma demande".

![Sélection de la catégorie](/images/Logiciels/web-marche/webmarche-selection-categorie.png)

 Choisissez parmi les questions posées afin de préciser votre demande.

![Choix parmi les questions](/images/Logiciels/web-marche/webmarche-choix.png)

Cas 1, la FAQ à répondu à votre demande. Cliquez sur "La FAQ m'a permis de résoudre mon problème".

![Cas 1](/images/Logiciels/web-marche/webmarche-cas-1.png)

Puis cliquez sur "Envoyer".

![Envoyer](/images/Logiciels/web-marche/webmarche-envoyer.png)

Cas 2, la FAQ ne vous a pas permis de résoudre votre demande. Cliquez sur "Je n'ai pas trouvé de réponse
satisfaisante, je poursuis ma demande".

![Cas 2](/images/Logiciels/web-marche/webmarche-cas-2.png)

Puis, expliquez votre demande en remplissant les champs avec les informations requises. (les champs avec unastérisque
sont obligatoires)

![Expliquer la demande](/images/Logiciels/web-marche/webmarche-expliquer-demande.png)

  Si vous êtes déjà connecté sur la plateforme avec vos identifiant alors les informations suivantes sont déjà
pré-renseignées.

Sinon, renseignez vos coordonnées et cliquez sur "Envoyer". (les champs avec un astérisque sont obligatoires)

![Renseignement des coordonnées](/images/Logiciels/web-marche/webmarche-renseigner-coordonnées.png)

Le bouton "Envoyer" devient vert, votre demande à été transmise à l’assistance.

Une fois la demande d'assistance ainsi créée, vous disposez de son identifiant. C'est une chaîne alphanumérique à
rappeler dans tous vos échanges. Cela permet d'optimiser son traitement et la résolution de votre problème.
