+++
title = "Premiers pas sur PubliS2low"
description = "Premiers pas sur PubliS2low \"Publis2low\" ADULLACT"
tags = [ "Publis2low" ]
date = "2024-01-26"
geekdocCollapseSection = true
+++
{{< toc >}}

Le service [https://publis2low.adullact.org](https://publis2low.adullact.org "Service PubliS2low de l'ADULLACT") est une
instance du logiciel libre PubliS2low dédiée aux collectivités et opérée par [l'ADULLACT](https://adullact.org "Site web de l'ADULLACT"). Les données sont hébergées en France.

## Pré-requis

Comme [tous les autres services](https://adullact.org/services-web "Présentations des services de l'ADULLACT"), le
service PubliS2low de l'ADULLACT s'adresse aux **adhérents de l'association**.
Pour demander la création d'un compte administrateur de votre collectivité adhérente, il
convient de renseigner le
[formulaire de demande d'accès PubliS2low de
l'ADULLACT](https://demarches.adullact.org/commencer/inscription-au-service-publis2low-de-l-adullact).

### la collectivité

* disposera d'une page de registre qui lui est dédiée sur laquelle sera publiée les actes règlementaires des 365 derniers jours. (la législation en vigueur faisant état de 2 mois de publication à minima)
* ne disposera pas des actes individuels / non règlementaires sur ce registre. Celui-ci étant public, les actes individuels ont été écartés lors de la récupération des données issues de S2low en conformité avec le RGPD en vigueur.

### l'administrateur de collectivité

* disposera d'un compte pour son/sa organisme/ collectivité.
* sera inscrit sur une liste de diffusion dédiée à PubliS2low. Cette liste est utilisée par l'ADULLACT pour informer
  des évolutions du service, informer nos utilisateurs en cas d'incident, échanger entres collectivités sur les usages
  autour du PubliS2low.

## Informations Pratiques à l'utilisation de PubliS2low

### Une fois mon inscription acceptée, comment est-ce que je récupère mes accès ?

Les accès se récupèrent depuis votre formulaire de demande d'accès au service.

### A quoi me servira la page de gestion ?

L'accès à la page de gestion vous permettra de :

* Récupérer l'url à intégrer sur vos sites internet. Privilégiez l'url lien iframe (qui termine en '&embedded')
* Ajouter des actes non issus de S2low
* Supprimer un acte de PubliS2low

## Utilisation au préalable du logiciel S2low

Le logiciel PubliS2low nécessite au préalable l'utilisation du logiciel S2low par votre collectivité.
[Plus d'informations](https://faq.adullact.org/services-adullact/s2low/)

La plateforme publis2low.adullact.org est connectée par API à la plateforme s2low.org

## Liste de discussions

Quand une collectivité demande un accès au service PubliS2low de l’ADULLACT, nous l’inscrivons sur une liste de diffusion dédiée à ce service. Il est possible d’y échanger entre pairs sur la publication des actes.

L’association ADULLACT utilise aussi cette liste pour informer les utilisateurs d’éventuels incidents et des évolutions fonctionnelles, ainsi que pour organiser des Groupes de Travail Collaboratifs (GTC).
