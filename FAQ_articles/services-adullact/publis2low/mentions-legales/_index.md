+++
title = "Mentions Légales"
description = "Mentions-legales \"Publis2low\" ADULLACT"
tags = [ "Publis2low" ]
date = "2024-01-26"
geekdocCollapseSection = true
+++

## Mentions légales du service publis2low.adullact.org

### Éditeur

[ADULLACT](https://adullact.org/), 5 rue du plan du palais - 34000 Montpellier

* Tel : 04 67 65 05 88
* Mail : `contact AROBASE adullact.org`
* RCS : Montpellier 443783170

### Directeur de la publication

Matthieu FAURE, Délégué Général de l'**ADULLACT**

### Prestataire d'hébergement

[OVH](https://www.ovhcloud.com/fr/)
