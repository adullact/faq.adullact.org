+++
title = "Accessibilité"
description = "Déclaration d'accessibilité du service \"Publis2low\" ADULLACT"
tags = [ "Publis2low" ]
date = "2024-01-26"
geekdocCollapseSection = true
+++


## Déclaration d'accessibilité du service publis2low.adullact.org


Cette déclaration d’accessibilité s’applique à [publis2low.adullact.org](https://publis2low.adullact.org/).

{{< toc >}}



### État de conformité

Le site [publis2low.adullact.org](https://publis2low.adullact.org/)  est **non conforme**
avec le _Référentiel Général d’amélioration de l’accessibilité_, RGAA version 4,
car il n’existe aucun résultat d’audit en cours de validité permettant de mesurer le respect des critères.

#### Résultats des tests

En l’absence d’audit de conformité il n’y a pas de résultats de tests.

#### Critères non conformes

En l’absence d’audit tous les critères du RGAA seront considérés comme non conformes par hypothèse.

#### Pages du site ayant fait l’objet de la vérification de conformité

En l’absence d’audit aucune page n’a fait l’objet de la vérification de conformité.

#### Outils pour évaluer l’accessibilité

En l’absence d’audit aucun outil n’a été utilisé lors de l’évaluation.

### Technologies utilisées

* HTML
* CSS
* JS
* PDF

### Établissement de cette déclaration d’accessibilité

Cette déclaration a été établie le **31/03/2024**.

### Retour d’information et contact

Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez contacter
le responsable du site web [publis2low.adullact.org](https://publis2low.adullact.org/) pour être orienté vers une alternative accessible
ou obtenir le contenu sous une autre forme.

* Envoyer un message à `support AROBASE adullact.org`
* Envoyer un courrier par la poste :

```text
  ADULLACT
  5 rue du plan du palais
  34000 Montpellier
```

### Voies de recours

Cette procédure est à utiliser dans le cas suivant.

Vous avez signalé au responsable du site internet un défaut d’accessibilité
qui vous empêche d’accéder à un contenu ou à un des services du portail
et vous n’avez pas obtenu de réponse satisfaisante.

* [Écrire un message au Défenseur des droits](https://formulaire.defenseurdesdroits.fr)
* [Contacter le délégué du Défenseur des droits dans votre région](https://www.defenseurdesdroits.fr/saisir/delegues)
* Envoyer un courrier par la poste (gratuit, ne pas mettre de timbre)

```text
  Défenseur des droits
  Libre réponse 71120
  75342 Paris CEDEX 07
```
