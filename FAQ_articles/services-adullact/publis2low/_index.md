+++
title = "PubliS2low"
tags = [ "Publis2low" ]
geekdocCollapseSection = true
+++
{{< toc-tree >}}

Le logiciel PubliS2low est un registre de publication des actes des collectivités territoriales.

Il intervient dans le cadre réglementaire de [publicité des actes des collectivités](https://www.collectivites-locales.gouv.fr/publicite-et-entree-en-vigueur-des-actes-des-collectivites-locales) entré en vigueur au 1er juillet 2022.

