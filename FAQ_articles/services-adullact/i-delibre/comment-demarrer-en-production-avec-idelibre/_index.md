+++
title = "Premiers pas avec i-delibRE"
description = "Premiers pas avec i-delibRE"
tags = [ "i-delibRE", "Assistance", "Premiers pas" ]
date = "2020-03-26"
+++
{{< toc >}}

Une personne de la collectivité ou de l'organisme doit être désignée pour gérer le service.
Une fois le [formulaire de demande d'ouverture d'accès à i-delibRE](https://demarches.adullact.org/commencer/inscription-au-service-i-delibre-de-l-adullact)
renseigné, nous lui créons un compte administrateur. Ce compte dispose de toutes les habilitations
nécessaires pour faire toutes les autres actions pour la collectivité.

Quand tout est prêt, vous recevrez un mail produit par la procédure d'oublie de mot de passe.
Ce message contient un lien pour pouvoir fixer le mot de passe. Le lien n'est valable que 24h.

Et pour démarrer concrètement et en production avec **i-delibRE**, voir les
[pré-requis techniques](../pre-requis-idelibre/).

Enfin, nous vous conseillons vivement (même si c'est optionnel) de consulter
les prestataires déclarés pour bénéficier d'un accompagnement professionnel.
Voir la [liste des prestataires **i-delibRE**](https://comptoir-du-libre.org/fr/softwares/servicesProviders/15 "Liste des prestataires").
