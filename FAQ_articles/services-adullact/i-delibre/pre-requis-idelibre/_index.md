+++
title = "Prérequis à l'utilisation d'i-delibRE (tablette)"
description = "Prérequis à l'utilisation d'i-delibRE (tablette)"
tags = [ "i-delibRE", "Premiers pas", "Tablette" ]
date = "2014-05-26"
+++
{{< toc >}}

Quels sont les pré-requis techniques pour utiliser i-Delibre ?

## Côté tablette

Pré-requis techniques minimaux requis pour utiliser I-delibre sur une tablette:

| Type de tablette | Processeur requis | RAM minimale | Version OS |
|------------------|-------------------|--------------|------------|
| Tablette **ANDROID** | Dual-core 1.2GHz / Cortex A9 _**ou**_ supérieur | 1 Go | >= 4.4 |
| Tablette **IPAD** | A4 1GHz _**ou**_ supérieur | 256 Mo | >= iOS 9 |
| Tablette **WINDOWS** | Intel 1.20 GHz | 1 Go | >= Microsoft Windows 8.1 |
  
### Taille préconisée pour les tablettes >= 10"  
  
Exemple de tablettes testées :

| Type de tablette | Modèles |
|------------------|---------|
| Tablette **ANDROÏD** | Samsung Galaxy Tab 2 _**ou**_ Asus Transformer Book Trio TX201TA-CQ003H 11" |
| Tablette **IPAD** | iPad 2 _**ou**_ iPad 3 |
| Tablette **WINDOWS** | Microsoft Surface Pro 10" _**ou**_ Microsoft Surface Pro 2 10" _**ou**_ Sony Vaio Tap 11" _**ou**_ HP 11-H060EF 11,6" |

Les technologies utilisées dans idelibre sont:

* Jquery 2.0.0
* Bootstrap 2.2.2
* Raphaël JS 2.1.0
* HTML5
* cache Manifest

L'application i-delibre st accessible selon votre tablette ici:

* ANDROID
  i-delibre est disponible sur plusieurs magasins android:
  * Google Play \
      [![i-delibRE / Android](/images/Logiciels/idelibre/logo-android.png "i-delibRE / Android")](https://play.google.com/store/apps/details?id=coop.adullact_projet.i_delibre)
* FILEDIR.com \
      ![i-delibRE / Filedir](/images/Logiciels/idelibre/logo-filedir.png "i-delibRE / Filedir")
* IOS sur apple store.
* WINDOWS sur microsoft store.

Un certificat compatible sera nécessaire côté serveur (voir chapitre ci-dessous) pour permettre la connexion de la
tablette. La configuration de ce certificat est importante: l'application tablette affiche le champ "O" (Organisation
name) du certificat.

## Côté client WEB (navigateur)

Navigateurs requis pour utiliser I-delibre sur un PC en mode WEB:

| Navigateur | Compatibilité idelibre |
|------------|------------------------|
| Chromium 29+ | OK |
| Chrome 29+ | OK |
| Firefox 21+ | OK |
| IE 7 | KO |
| IE 8 | KO |
| IE 9 | KO |
| IE 10 | OK |
| Opéra Coast | OK |
| Safari 5.1.9+ | OK |

## Côté serveur

Pour le serveur, les pré-requis matériels sont :

* 2 Go de Ram
* Un espace disque de 30Go
* 2 CPU >= 1Ghz

et pour la partie logicielle:

* Ubuntu 14.04 LTS x64
* Ouverture des ports 80 et 443
* Apache 2
  * mod_expires
  * mod_rewrite
  * mod_ssl
* PHP 5.4
* Postgresql 9.1
* CakePHP v. 2.4.1
* Ghostscript (>= 8.71)

De plus, un certificat HTTPS est impératif côté serveur afin de garantir la sécurité entre les tablettes et le serveur.
Ce certificat est dédié à une collectivité ou un ensemble de collectivités exploitant ce serveur.
