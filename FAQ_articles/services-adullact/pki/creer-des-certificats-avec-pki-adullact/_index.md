+++
title = "Comment obtenir votre certificat logiciel pour S2low ?"
description = "ADULLACT permet à ses adhérents d'obtenir leurs propres certificats logiciel pour S2low."
tags = [ "PKI", "Certificat" ]
date = "2010-09-09"
+++
{{< toc >}}

## Comment et pourquoi obtenir un certificat logiciel X.509 ?

L'ADULLACT permet à ses adhérents d'obtenir leurs propres certificats logiciel X.509 issue d'une [autorité de certification Adullact](https://pki.adullact.org/certification-authority).

Ces certificats permettent à nos adhérent de pouvoir administrer leur collectivité dans S2low. Les tâches d'administration dans S2low consistent par exemple en :

* la création d'autres comptes pour vôtre collectivité
* la mise à jour de la classification.

Ces certificats X.509 logiciel ne permettent pas de télétransmettre.

Pour obtenir un accès à notre infrastructure de gestion de clefs, une collectivité adhérente doit renseigner une [demande d'accès à notre PKI](https://demarches.adullact.org/commencer/demande-d-acces-au-service-pki-de-l-adullact).

Une fois la demande d'accès instruite, le gestionnaire des clefs pourra produire autant de certificat X.509 que souhaité.
Pour chaque création de certificat par le gestionnaire, l'adresse mail de l'utilisateur final indiquée recevra le certificat X.509 au format PKCS12.
Ce certificat pourra s'importer dans le magasin à certificats du poste de travail de l'utilisateur final, administrateur S2low.

## Au sens de la norme X.509, un certificat pour quels usages ?

Aujourd'hui, au sens de la norme X.509 v3, deux usages sont embarqués dans nos certificats :

* `TLS Web Client Authentication` : cet usage permet, par exemple, au navigateur web de se connecter sur la plateforme S2low.org.
* `Digital Signature` : cet usage permet, par exemple, la signature électronique dans un contexte de parapheur.

## Quelle est notre infrastructure de gestion de clefs ?

L'Adullact utilise le [logiciel libre CFSSL](https://cfssl.org) comme moteur de ses autorités de certification. CFSSL est une sorte de couteau suisse, comme un SDK (Software Development Kit).

Pour s'interfacer avec une autorité de certification motorisé par CFSSL, l'Adullact a décidé de développer le [logiciel libre Tajine](https://gitlab.adullact.net/adullact/pki/tajine). Tajine est une autorité d'enregistrement qui donne un accès graphiques à la gestion des certificats.

Pour construire le service, nous avons produit plusieurs outils libres regroupés sur [la page du projet pki](https://gitlab.adullact.net/adullact/pki/). Ces outils peuvent vous aider si vous souhaitez construire votre propre infrastructure de gestion de clefs.
