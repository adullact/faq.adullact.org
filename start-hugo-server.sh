##
## Récupération et initialisation du thème
## (première utilisation ou mise à jour du thème)
##
# git submodule update --init --recursive
# cd .gitlab/website_hugo/themes/hugo-geekdoc-adullact/ || exit 1
# npm install --no-package-lock
# npx gulp default
# cd ../../../../ || exit 1

##
## Lancement du server hugo de développement
##
cd .gitlab/website_hugo/ || exit 1
hugo serve
