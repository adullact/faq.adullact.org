+++
title = "Archive - Articles dépubliés"
geekdocCollapseSection = true
expirydate = "2021-07-31"
+++

## Que contient ce dossier d'archive ?

Ce dossier d'archive contient tous les articles
**dépubliés** de manière massive et archivés ici.

## Comment dépublier un article ?

### Méthode 1 : utiliser le mode brouillon (`dratft = true`)

Dans l'en-tête du fichier (encadré par les balises `+++`),
il suffit d'y ajouter `dratft = true` pour que la page ne
soit plus visible en ligne.

> Pour afficher les articles en brouillon sur votre ordinateur :
> ```bash
> hugo serve --buildDrafts
> ```

### Méthode 2 : utiliser une date d'expiration  (`expirydate`)

Dans l'en-tête du fichier (encadré par les balises `+++`),
il suffit d'y ajouter `expirydate = "2021-07-31"`, avec une date du passé, pour que la page ne
soit plus visible en ligne.

C'est la méthode à privilégier pour dépublier un article de manière durable.

> Pour afficher sur votre ordinateur les articles dépubliés via une date d'expiration  :
> ```bash
> hugo serve --buildExpired
> ```

## Articles dépubliés et archivés ici

Tous ces articles une date d'**expiration**
ce qui permet de ne pas les publier sur le site web.

{{< toc-tree >}}

