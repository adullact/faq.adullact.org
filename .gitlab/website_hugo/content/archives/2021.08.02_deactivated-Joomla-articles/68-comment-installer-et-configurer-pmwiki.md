+++
title = "Comment installer et configurer PmWiki ?"
description = ""
tags = [ "pmWiki" ]
date = "2006-07-05"
expirydate = "2021-07-31"
+++
{{< toc >}}

[PmWiki](http://www.pmwiki.org/) est écrit en PHP et distribué sous licence GPL. Il est conçu pour être simple à
installer, modifier, et entretenir pour différents usages.

## 1\. Installation de l'application

Télécharger la dernière version de [pmWiki](http://www.pmwiki.org/wiki/PmWiki/Download)

1. Dézipper l'archive dans votre DocumentRoot
2. Créer le répertoire 'wiki.d' et lui donner les droits d'écriture à votre server web.

Sous unix :

`mkdir wiki.d`

`chmod ugo+w wiki.d`

## 2\. Configurer l'application pour le français

Télécharger le fichier i18.tgz

Décompresser le fichier.

Placer les fichiers obtenus dans les répertoires adéquats `wikilib.d/` et `scripts/`

Créer le fichier config.php dans le répertoire local/

Ecrire dans ce fichier les lignes suivantes afin de le configurer en français :

`<?`

`XLPage('fr','PmWikiFr.XLPage');`

`>`

## 3\. Lutte anti-spam

Fixer un mot de passe à la page en cours :

Une des façons les plus efficace pour lutter contre le spam sur votre PmWiki est de protéger par un mot de passe votre
page, nous allons voir à travers cet example comment rendre possible cette manipulation. Nous copions le lien de la
page lorsqu'on clique sur le bouton "Edit":

_<http://test.adullact.net/wiki/pmwiki.php?n=PAGE.TEST0001?action=edit>_

Nous remplaçons cette URL par:

_<http://test.adullact.net/wiki/pmwiki.php?n=PAGE.TEST0001?action=attr>_

Une nouvelle page apparaît et vous permet de fixer un mot de passe. Lors des prochains clics sur le bouton "Edit" de
cette page un mot de passe vous sera demandé, c'est bien ce que nous voulons faire, protéger par un mot de passe une
page de notre PmWiki.

Forcer le wiki à demander un nom d'auteur à la modification ou à la création d'une page :

PmWiki offre différents moyens de lutter contre la pollution de votre wiki par du spam.

Depuis la version > pmwiki-2.1.0, il existe un outil intégré forçant le rédacteur à saisir son login.

Ce procédé bloque les robots qui insèrent des spams automatiquement sur les wikis. Il suffit d'ajouter la ligne
suivante au fichier _local/config.php_ : `$EnablePostAuthorRequired = 1;`

Filtrer les éditions par IP et par mots clefs :

Il existe d'autres outils comme [Blocklist2](http://www.pmwiki.org/wiki/Cookbook/Blocklist2)

Pour installer ce module, il suffit de copier le ficher Blocklist2.php dans votre répertoire `cookbook/`.

Compléter ensuite votre fichier de configuration _local/config.php_ en y ajoutant la ligne suivante :

`if ($action=='edit') include_once($FarmD.'/cookbook/blocklist2.php');`

Créer une page Site.Blocklist contenant les adresses IP à bannir, les mots à censurer.

Vous trouverez une page d'exemple en cliquant ici que vous pouvez utiliser et que vous pourrez compléter par la suite.

## 4\. Supprimer l'historique

Si votre wiki a été spammé par des taggueurs, il existe un moyen de supprimer l'historique, et ainsi supprimer les
traces de vandalisme. Copier le fichier expirediff.php dans le répertoire _cookbook_

Ajouter la ligne suivante dans votre fichier de configuration locale :

`local/config.php _include_once('cookbook/expirediff.php');_`

Dans l'url de la page où vous vous trouvez, et où vous voulez supprimer l'historique, ajoutez en fin d'url :
`?action=expirediff`
