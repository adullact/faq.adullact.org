# CI ~ Prerequisites for checks links (QA) with "linkchecker"

Docker image of prerequisites for **checks links** (internals + externals)
with [linkchecker](https://github.com/linkchecker/linkchecker).


## Usage in Gitlab CI configuration

 ```yaml
###############################################################
# linkchecker     https://linkchecker.github.io/linkchecker/
#                 https://github.com/linkchecker/linkchecker
###############################################################
linkchecker:
    stage: quality-checks
    image:
        name: gitlab.adullact.net:4567/adullact/faq.adullact.org/qa_linkchecker:v1.0.1
    variables:
        URL: "http://example.org/"
        EXTRA_ARG: "-v -r1 --check-extern --no-robots"
    script:
        - linkchecker --version
        - linkchecker ${EXTRA_ARG} "${URL}"
 ```

## To update Docker image

3 steps to update Docker image:
- Edit Dockerfile
- Build Docker image and push to container registry
- Update this README file

### Edit Dockerfile

Edit [Dockerfile](Dockerfile) according to your needs
and in addition you must to change in this file `LABEL version="1.0.1"`
following [semantic versioning](http://semver.org/) recommendations:

 ```shell script
 MAJOR.MINOR.PATCH
    # MAJOR ---> a breaking change (incompatible API changes)
    # MINOR ---> add a new feature
    # PATCH ---> fix a bug
```

```dockerfile
FROM debian:buster-slim
LABEL version="1.0.1"  \
      description="Prerequisites for checks links (internals + externals) with linkchecker"
```

### Build Docker image and push to container registry

Build new Docker image and push to [Gitlab container registry](https://gitlab.adullact.net/adullact/faq.adullact.org/container_registry)

```bash
# Configure variables
# in particular DOCKER_IMAGE_VERSION
# which must be identical to LABEL.version in Dockerfile
GITLAB_URI="gitlab.adullact.net:4567"
GITLAB_REPOSITORY="adullact/faq.adullact.org"
DOCKER_IMAGE_NAME="qa_linkchecker"
DOCKER_IMAGE_VERSION="v1.0.1" # must be identical to LABEL.version in Dockerfile

# Login to Gitlab
docker login "${GITLAB_URI}"

# Build new Docker image
DOCKER_IMAGE="${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"
docker build -t "${DOCKER_IMAGE}" .
docker image ls | grep "${DOCKER_IMAGE_NAME}"
docker image ls | grep "${DOCKER_IMAGE_NAME}" | grep "${DOCKER_IMAGE_VERSION}"

# Verify that new builded Docker image works
docker container run -t -i "${DOCKER_IMAGE}"
        # linkchecker --version
        # linkchecker -v -r1 --check-extern http://example.org/
        # exit

# Push to Gitlab container registry
docker image push "${DOCKER_IMAGE}"

# Logout to remove Gitlab credentials from $HOME/.docker/config.json file
docker logout "${GITLAB_URI}"

# Display new Docker image to use in the Gitlab CI configuration
echo "New Dcoker image to use in the Gitlab CI configuration:"; echo "${DOCKER_IMAGE}"
    # image: gitlab.adullact.net:4567/adullact/faq.adullact.org/qa_linkchecker:v1.0.1
```

### Update README

Replace `1.0.1` version by the new version `x.y.z` in this [README.md](README.md) file.
