# CI ~ Docker images of prerequisites

Docker images of prerequisites usign by **Gitlab CI**:
- [**Hugo** build with **hugo-geekdoc** theme](./build_hugo_prerequisites/)
- [**deploy** website with **rsync**](./deploy_rsync/)
- [QA - **Checks links**  with **linkchecker**](./QA_linkchecker/)
