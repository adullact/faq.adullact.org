# FAQ.adullact.org

Site web [faq.adullact.org](https://faq.adullact.org)  avec le générateur de site statique [Hugo](https://gohugo.io/).

Contenu du project :

- en **français** :
  - [documentation **Rédacteur**](Documentation/doc_pour-les-redacteurs/)
  - [**articles** du site web](FAQ_articles/)
  - [**images** des articles](FAQ_ressources/)
- en **anglais** :
  - [documentation **Développeur**](Documentation/doc_for-developer/)
  - configuration technique

## Documentation

...

## License

[AGPL v3](LICENSE)
